<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model
{
    protected static $defaultKey = "G1gh4";
    protected $table = 'apikey';
    protected $fillable = ['key'];

    public static function getDefaultKey()
    {
        return static::$defaultKey;
    }
}
