<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ApiKey;
use Illuminate\Support\Facades\Validator;

class ApiKeyController extends Controller
{
    public function __construct()
    {
        $Authorization = request()->header('Authorization');
        if ($Authorization != ApiKey::getDefaultKey()) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    public function create(Request $request)
    {
        $obj['key'] = $request->key;
        ApiKey::insert($obj);
        $response["Success"] = "1";
        $response["Value"] = "Success insert key";
        return response()->json($response, 201);
    }

    public function update(Request $request, $id = null)
    {
        if ($id == null) {
            $id = $request->id;
        }

        $obj['key'] = $request->key;
        ApiKey::where('id', '=', $id)->update($obj);
        $response["Success"] = "1";
        $response["Value"] = "Success update key";
        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $key = ApiKey::find($id);
        $key->delete();
        $response["Success"] = "1";
        $response["Value"] = "Success delete key";
        return response()->json($response, 201);
    }
}
