<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ApiKey;

class OperatorController extends Controller
{
    public function __construct()
    {
        $key = ApiKey::take(1)->first();
        $Authorization = request()->header('Authorization');
        if ($Authorization != $key->key) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    // This operator
    public function operator()
    {
        $db = DB::table('users');
        $operator = $db->where('role', '!=', 0)
            ->orderby('created_at', 'desc')
            ->get();

        if ($operator == []) {
            $response = [
                "Success"       => "0",
                "value"         => "Data operator kosong",
            ];
        } else {
            $response = [
                "Success"       => "1",
                "value"         => $operator,
            ];
        }

        return response()->json($response, 201);
    }

    public function create(Request $request)
    {
        $insert = DB::table('users')->insert([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => $request->password,
            'role'      => 1,

        ]);

        if (!$insert) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menambah operator.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menambah operator.",
            ];
        }

        return response()->json($response, 201);
    }

    public function update(Request $request, $id)
    {
        $update = DB::table('users')
            ->where('id', '=', $id)
            ->update([
                'name'      => $request->name,
                'email'     => $request->email,
                'password'  => $request->password,
                'role'      => 1,
            ]);

        if (!$update) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal mengubah operator.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil mengubah operator.",
            ];
        }

        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $delete = DB::table('users')
            ->where('id', '=', $id)
            ->delete();

        if (!$delete) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menghapus operator.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menghapus operator.",
            ];
        }

        return response()->json($response, 201);
    }
}
