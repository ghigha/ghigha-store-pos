<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ApiKey;

class PembeliController extends Controller
{

    public function __construct()
    {
        $key = ApiKey::take(1)->first();
        $Authorization = request()->header('Authorization');
        if ($Authorization != $key->key) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    // This pembeli
    public function pembeli()
    {
        $pembeli = DB::table('pembeli')->orderby('id', 'desc')->get();

        if ($pembeli == []) {
            $response = [
                "Success"       => "0",
                "Value"         => "Data pembeli kosong",
            ];
        } else {
            $response = [
                "Success"       => "1",
                "Value"         => $pembeli,
            ];
        }

        return response()->json($response, 201);
    }

    public function detail($id)
    {
        $pembeli = DB::table('pembeli')
            ->where('id', '=', $id)
            ->first();

        if ($pembeli == []) {
            $response = [
                "Success"       => "0",
                "Value"         => "Data pembeli kosong",
            ];
        } else {
            $response = [
                "Success"       => "1",
                "Value"         => $pembeli,
            ];
        }

        return response()->json($response, 201);
    }

    public function create(Request $request)
    {
        $insert = DB::table('pembeli')->insert([
            'id_user'       => $request->id_user,
            'nama_pembeli'  => $request->nama_pembeli,
            'nomor_hp'      => $request->nomor_hp,
            'alamat'        => $request->alamat,
            'provinsi'      => $request->provinsi,
            'kabupaten'     => $request->kabupaten,
            'kode_pos'      => $request->kode_pos,
        ]);

        if (!$insert) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menambah pembeli.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menambah pembeli.",
            ];
        }

        return response()->json($response, 201);
    }

    public function update(Request $request, $id)
    {
        $insert = DB::table('pembeli')
            ->where('id', '=', $id)
            ->update([
                'id_user'       => $request->id_user,
                'nama_pembeli'  => $request->nama_pembeli,
                'nomor_hp'      => $request->nomor_hp,
                'alamat'        => $request->alamat,
                'provinsi'      => $request->provinsi,
                'kabupaten'     => $request->kabupaten,
                'kode_pos'      => $request->kode_pos,
            ]);

        if (!$insert) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal mengubah pembeli.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil mengubah pembeli.",
            ];
        }

        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $delete = DB::table('pembeli')
            ->where('id', '=', $id)
            ->delete();

        if (!$delete) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menghapus pembeli.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menghapus pembeli.",
            ];
        }

        return response()->json($response, 201);
    }
    // .....................
}
