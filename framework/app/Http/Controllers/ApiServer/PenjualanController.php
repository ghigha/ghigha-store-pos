<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ApiKey;

class PenjualanController extends Controller
{

    public function __construct()
    {
        $key = ApiKey::take(1)->first();
        $Authorization = request()->header('Authorization');
        if ($Authorization != $key->key) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    // This data penjualan
    public function penjualan()
    {
        $penjualan = DB::table('transaksi')
            ->join('pembeli', 'pembeli.id', '=', 'transaksi.id_pembeli')
            ->join('stat', 'stat.id', '=', 'transaksi.id_stat')
            ->select('transaksi.*', 'stat.nama_stat', 'pembeli.nama_pembeli', 'pembeli.nomor_hp')
            ->orderby('transaksi.created_at', 'desc')
            ->get();

        if ($penjualan == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data penjualan kosong",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $penjualan,
            ];
        }

        return response()->json($response, 201);
    }

    public function detail($id)
    {
        $id_penjualan = $id;
        $penjualan = DB::table('transaksi')
            ->join('pembeli', 'pembeli.id', '=', 'transaksi.id_pembeli')
            ->join('stat', 'stat.id', '=', 'transaksi.id_stat')
            ->join('users', 'users.id', '=', 'transaksi.id_user')
            ->join('method_pem', 'method_pem.id', '=', 'transaksi.id_method')
            ->select('transaksi.*', 'stat.nama_stat', 'users.name', 'method_pem.nama_method_pem', 'pembeli.nama_pembeli', 'pembeli.nomor_hp', 'pembeli.alamat', 'pembeli.kode_pos')
            ->where('transaksi.id', $id_penjualan)
            ->first();
        $produk = DB::table('detail_transaksi')
            ->join('produk', 'detail_transaksi.id_produk', 'produk.id')
            ->where('id_transaksi', $id_penjualan)
            ->orderby('detail_transaksi.id', 'asc')
            ->get();
        $stats = DB::table('stat')->orderby('id', 'asc')->get();

        if ($penjualan == []) {
            $response["Success"]    = "0";
            $response["Value"]      = "Data penjualan kosong";
        } else {
            $response["Success"]    = "1";
            $response["Value"]      = $penjualan;
            if (!$produk == []) {
                $response["produk"]     = $produk;
            }

            if (!$stats == []) {
                $response["stats"]      = $stats;
            }
        }

        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $delete_detail_transaksi = DB::table('detail_transaksi')->where('id_transaksi', $id)->delete();
        if ($delete_detail_transaksi) {
            $delete = DB::table('transaksi')->where('id', $id)->delete();
        } else {
            $delete = false;
        }

        if (!$delete) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menghapus operator.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menghapus operator.",
            ];
        }

        return response()->json($response, 201);
    }
}
