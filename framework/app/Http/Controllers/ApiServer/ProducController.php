<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\ApiKey;

class ProducController extends Controller
{

    public function __construct()
    {
        $key = ApiKey::take(1)->first();
        $Authorization = request()->header('Authorization');
        if ($Authorization != $key->key) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    // This produk
    // .................
    public function produk()
    {
        $db = DB::table('produk')
            ->join('variant', 'variant.id', '=', 'produk.id_variant')
            ->select('variant.nama_variant', 'variant.warna', 'variant.size', 'produk.*');
        $produk = $db->orderby('produk.created_at', 'desc')->get();

        if ($produk == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data produk kosong"
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $produk
            ];
        }

        return response()->json($response, 201);
    }

    public function detail($id)
    {

        $id_produk = $id;
        $produk = DB::table('produk')
            ->join('supplier', 'supplier.id', '=', 'produk.id_supplier')
            ->join('variant', 'variant.id', '=', 'produk.id_variant')
            ->select('supplier.nama_supplier', 'variant.*', 'produk.*')
            ->where('produk.id', $id_produk)
            ->first();

        if ($produk == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data Produk Kosong"
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $produk
            ];
        }

        return response()->json($response, 201);
    }

    public function create(Request $request)
    {
        $insert = DB::table('produk')->insert([
            'id_supplier'       => $request->id_supplier,
            'id_variant'        => $request->id_variant,
            'nama_produk'       => $request->id_produk,
            'harga'             => $request->harga,
            'harga_beli'        => $request->stok,
            'stok'              => $request->stok,
        ]);

        if (!$insert) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menambah produk.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menambah produk.",
            ];
        }

        return response()->json($response, 201);
    }

    public function update(Request $request, $id)
    {
        $update = DB::table('produk')
            ->where('id', '=', $id)
            ->update([
                'id_supplier'       => $request->id_supplier,
                'id_variant'        => $request->id_variant,
                'nama_produk'       => $request->id_produk,
                'harga'             => $request->harga,
                'harga_beli'        => $request->stok,
                'stok'              => $request->stok,
            ]);

        if (!$update) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal mengubah produk.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil mengubah produk.",
            ];
        }

        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $delete = DB::table('produk')
            ->where('id', '=', $id)
            ->delete();

        if (!$delete) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menghapus produk.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menghapus produk.",
            ];
        }

        return response()->json($response, 201);
    }
    // .....................
}
