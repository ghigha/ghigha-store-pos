<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ApiKey;

class SupplierController extends Controller
{

    public function __construct()
    {
        $key = ApiKey::take(1)->first();
        $Authorization = request()->header('Authorization');
        if ($Authorization != $key->key) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    // This supplier
    public function supplier()
    {
        $db = DB::table('supplier');
        $supplier = $db->orderby('created_at', 'desc')->get();

        if ($supplier == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data supplier kosong",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $supplier,
            ];
        }

        return response()->json($response, 201);
    }

    public function detail($id)
    {
        $supplier = DB::table('supplier')->where('id', '=', $id)->first();

        if ($supplier == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data supplier kosong",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $supplier,
            ];
        }
        return response()->json($response, 201);
    }

    public function create(Request $request)
    {
        $insert = DB::table('supplier')->insert([
            'nama_supplier' => $request->nama_supplier,
        ]);

        if (!$insert) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menambah supplier.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menambah supplier.",
            ];
        }

        return response()->json($response, 201);
    }

    public function update(Request $request, $id)
    {
        $update = DB::table('supplier')
            ->where('id', '=', $id)
            ->update([
                'nama_supplier' => $request->nama_supplier,
            ]);

        if (!$update) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal mengubah supplier.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil mengubah supplier.",
            ];
        }

        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $delete = DB::table('supplier')
            ->where('id', '=', $id)
            ->delete();

        if (!$delete) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menghapus supplier.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menghapus supplier.",
            ];
        }

        return response()->json($response, 201);
    }
    // .....................
}
