<?php

namespace App\Http\Controllers\ApiServer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ApiKey;

class VarianController extends Controller
{

    public function __construct()
    {
        $key = ApiKey::take(1)->first();
        $Authorization = request()->header('Authorization');
        if ($Authorization != $key->key) {
            $response["Success"] = "0";
            $response["Value"] = "Header Authorization Tidak Sama";
            echo response()->json($response, 404);
            die;
        }
    }

    // This varian
    public function varian()
    {
        $db = DB::table('variant');
        $varian = $db->orderby('created_at', 'desc')->get();

        if ($varian == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data varian kosong",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $varian,
            ];
        }

        return response()->json($response, 201);
    }

    public function detail($id)
    {
        $db = DB::table('variant');
        $varian = $db->where('id', '=', $id)->first();

        if ($varian == []) {
            $response = [
                "Success"   => "0",
                "Value"     => "Data varian tidak ditemukan",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => $varian,
            ];
        }

        return response()->json($response, 201);
    }

    public function create(Request $request)
    {
        $insert = DB::table('variant')->insert([
            'nama_variant'  => $request->nama_variant,
            'warna'         => $request->warna,
            'size'          => $request->size,
        ]);

        if (!$insert) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menambah variant.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menambah variant.",
            ];
        }

        return response()->json($response, 201);
    }

    public function update(Request $request, $id)
    {
        $update = DB::table('variant')
            ->where('id', '=', $id)
            ->update([
                'nama_variant'  => $request->nama_variant,
                'warna'         => $request->warna,
                'size'          => $request->size,
            ]);

        if (!$update) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal mengubah variant.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil mengubah variant.",
            ];
        }

        return response()->json($response, 201);
    }

    public function delete($id)
    {
        $delete = DB::table('variant')
            ->where('id', '=', $id)
            ->delete();

        if (!$delete) {
            $response = [
                "Success"   => "0",
                "Value"     => "Gagal menghapus variant.",
            ];
        } else {
            $response = [
                "Success"   => "1",
                "Value"     => "Berhasil menghapus variant.",
            ];
        }

        return response()->json($response, 201);
    }
}
