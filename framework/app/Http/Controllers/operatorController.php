<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Crypt;
use Hash;
use DB;

class operatorController extends Controller
{
    public function index(Request $request)
    {
        $key = $request->get('key');
        $db = DB::table('users');
        if($key !== null){
            $db->orWhere('name','like','%'.$key.'%')->orWhere('email','like','%'.$key.'%');
        }
        $operator = $db->where('role','!=', 0)
                       ->orderby('created_at', 'desc')
                       ->get();
        return view('admin.operator.index', ['operator' => $operator]);
    }

    public function add()
    {
        return view('admin.operator.add');
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'password' => 'required|string|min:8|confirmed',
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|string|max:255',
        ]);
        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 1,
        ]);
        
        return redirect()->route('operator')->with('sukses', 'Berhasil menambahkan operator');
    }

    public function delete($id)
    {
       DB::table('users')
       ->where('id','=', Crypt::decrypt($id))
       ->delete();

       return redirect()->route('operator')->with('sukses', 'Berhasil Menghapus operator');
    }
}
