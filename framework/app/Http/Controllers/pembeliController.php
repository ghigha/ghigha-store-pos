<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Crypt;

class pembeliController extends Controller
{
    public function index()
    {
        $pembeli = DB::table('pembeli')->orderby('id','desc')->get();
        return view('operator.pembeli.index', ['pembeli' => $pembeli]);
    }

    public function add() 
    {
        return view('operator.pembeli.add');
    }

    public function edit($id) 
    {
        $id_pembeli = Crypt::decrypt($id);
        $pembeli = DB::table('pembeli')
                    ->where('pembeli.id', $id_pembeli)
                    ->first();
        return view('operator.pembeli.edit', ['pembeli' => $pembeli]);
    }
    
    public function store(Request $request)
    {
        $produk = DB::table('pembeli');
        $produk->insert($request->except(['_token']));

        return redirect()->route('pembeli')->with('sukses', 'Berhasil menambah pembeli');
    }

    public function update(Request $request, $id)
    {
        $id_pembeli = Crypt::decrypt($id);
        DB::table('pembeli')
        ->where('id', $id_pembeli)
        ->update($request->except(['_token','_method']));
        return redirect()->route('pembeli.detail',Crypt::encrypt($id_pembeli))->with('sukses', 'Berhasil mengubah pembeli');
    }

    public function delete($id){
        $id_pembeli = Crypt::decrypt($id);
        $transaksi = DB::table('transaksi')->where('id_pembeli', $id_pembeli);
        $data_transaksi = $transaksi->get();
        foreach ($data_transaksi as $tran) {
            DB::table('detail_transaksi')->where('id_transaksi', $tran->id)->delete();
        } 
        $transaksi->delete();
        DB::table('pembeli')
        ->where('id','=', $id_pembeli)
        ->delete();
        return redirect()->route('pembeli')->with('sukses', 'Berhasil Menghapus pembeli');
    }

    public function detail($id)
    {
        $id_pembeli = Crypt::decrypt($id);
        $pembeli = DB::table('pembeli')
                    ->join('users','users.id', '=', 'pembeli.id_user')
                    ->select('users.name','pembeli.*')
                    ->where('pembeli.id', $id_pembeli)
                    ->first();
        return view('operator.pembeli.detail', ['pembeli' => $pembeli]);
    }
}

