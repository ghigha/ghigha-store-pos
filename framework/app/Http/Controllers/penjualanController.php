<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Crypt;

class penjualanController extends Controller
{
    public function index()
    {
        $penjualan = DB::table('detail_transaksi')
                    ->join('transaksi', 'transaksi.id','=','detail_transaksi.id_transaksi')
                    ->join('produk', 'produk.id','=','detail_transaksi.id_produk')
                    ->join('variant', 'produk.id_variant', '=', 'variant.id')
                    ->join('pembeli', 'pembeli.id','=','transaksi.id_pembeli')
                    ->join('stat', 'stat.id','=','transaksi.id_stat')
                    ->select('transaksi.*', 'variant.*','stat.nama_stat', 'pembeli.nama_pembeli', 'produk.nama_produk', 'detail_transaksi.qty', 'detail_transaksi.id_transaksi', 'pembeli.nomor_hp')
                    ->orderby('transaksi.created_at', 'desc')
                    ->get();
        return view('operator.penjualan.index', ['penjualan'=>$penjualan]);
    }

    public function detail($id)
    {
        $id_penjualan = Crypt::decrypt($id);
        $penjualan = DB::table('transaksi')
                    ->join('pembeli', 'pembeli.id','=','transaksi.id_pembeli')
                    ->join('stat', 'stat.id','=','transaksi.id_stat')
                    ->join('users', 'users.id','=','transaksi.id_user')
                    ->join('method_pem', 'method_pem.id','=','transaksi.id_method')
                    ->select('transaksi.*','stat.nama_stat', 'users.name', 'method_pem.nama_method_pem', 'pembeli.nama_pembeli', 'pembeli.nomor_hp', 'pembeli.alamat', 'pembeli.kode_pos')
                    ->where('transaksi.id', $id_penjualan)
                    ->first();
        $produk = DB::table('detail_transaksi')
                    ->join('produk', 'detail_transaksi.id_produk', 'produk.id')
                    ->join('variant', 'variant.id', 'produk.id_variant')
                    ->where('id_transaksi', $id_penjualan)
                    ->orderby('detail_transaksi.id','asc')
                    ->get();
        $stats = DB::table('stat')->orderby('id', 'asc')->get();

        return view('operator.penjualan.detail', ['penjualan'  => $penjualan, 'produk' => $produk, 'stats' => $stats]);
    }

    public function add(Request $request)
    {   
        $expedisi = $request->get('expedisi');
        $ongkir = $request->get('ongkir') === null ? 0 : $request->get('ongkir');
        $stats = DB::table('stat')->orderby('id', 'asc')->get();
        $pembeli = DB::table('pembeli')->orderby('nama_pembeli', 'asc')->get();
        $produk = DB::table('produk')
        ->join('variant','variant.id','=','produk.id_variant')
        ->select('variant.*','produk.*')
        ->where('stok', '>=', 1)->orderby('nama_produk', 'asc')->get();
        return view('operator.penjualan.add', ['produk' => $produk, 'pembeli' => $pembeli, 'stats' => $stats, 'ongkir' => $ongkir, 'expedisi' => $expedisi]);
    }

    public function edit($id)
    {
        $id_penjualan = Crypt::decrypt($id);
        $pembeli = DB::table('pembeli')->orderby('nama_pembeli', 'asc')->get();
        $penjualan = DB::table('transaksi')
                    ->where('transaksi.id', $id_penjualan)
                    ->first();
        $stats = DB::table('stat')->orderby('id', 'asc')->get();

        return view('operator.penjualan.edit', ['penjualan'  => $penjualan, 'stats' => $stats, 'pembeli' => $pembeli]);
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'id_produk' => 'required',
        ],['id_produk.required' => 'Pilih minimal satu produk']);
        $no_pesanan = array('no_pesanan' => 'GS-'.date('siHdmY'), 'id_user' => Auth::user()->id);
        $transaksi = DB::table('transaksi');
        $transaksi->insert(array_merge($request->except(['_token','id_produk','qty']), $no_pesanan));
        $id = $transaksi->where('no_pesanan', $no_pesanan['no_pesanan'])->first();

        for($i=0;$i<count($request->id_produk);$i++)
        {
            DB::table('detail_transaksi')->insert([
                'id_produk' => $request->id_produk[$i],
                'qty' => $request->qty[$i],
                'id_transaksi' => $id->id
            ]);
            DB::table('produk')
                ->where('id', $request->id_produk[$i])
                ->update(['stok' => DB::raw('stok - '.$request->qty[$i])]);
        }
        return redirect()->route('penjualan')->with('sukses', 'Berhasil menambah penjualan');
    }

    public function update(Request $request, $id)
    {
        $id_penjualan = Crypt::decrypt($id);
        $b_admin = $request->admin - $request->prev_admin;
        $b_ongkir = $request->ongkir - $request->prev_ongkir;
        $hasil = $b_admin + $b_ongkir;
        $arr = array('jumlah_total' => DB::raw('jumlah_total + '.$hasil));
        DB::table('transaksi')
        ->where('id', $id_penjualan)
        ->update(array_merge($request->except(['_token','_method', 'prev_admin', 'prev_ongkir']), $arr));

        return redirect()->route('penjualan.detail', $id)->with('sukses', 'Berhasil mengubah data penjualan');
    }

    public function update_stat(Request $request, $id)
    {
        $id_penjualan = Crypt::decrypt($id);
        DB::table('transaksi')->where('id', $id_penjualan)->update(['id_stat' => $request->id_stat]);

        return redirect()->back()->with('sukses', 'Berhasil mengubah status pemesanan');
    }

    public function delete($id){
        $id_penjualan = Crypt::decrypt($id);
        DB::table('detail_transaksi')->where('id_transaksi', $id_penjualan)->delete();
        DB::table('transaksi')->where('id', $id_penjualan)->delete();

        return redirect()->route('penjualan')->with('sukses', 'Berhasil Menghapus penjualan');
    }
}
