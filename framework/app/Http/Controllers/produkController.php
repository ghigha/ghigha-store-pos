<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Crypt;
use Auth;

class produkController extends Controller
{
    public function index(Request $request)
    {
        $key = $request->get('key');
        $db = DB::table('produk')
                ->join('variant','variant.id','=','produk.id_variant')
                ->select('variant.nama_variant','variant.warna','variant.size','produk.*');
        if($key !== null){
            $db->orWhere('nama_produk','like','%'.$key.'%');
        }
        $produk = $db->orderby('created_at', 'desc')->get();
        return view('admin.produk.index', ['produk' => $produk]);
    }

    public function detail($id)
    {
        $id_produk = Crypt::decrypt($id);
        $produk = DB::table('produk')
                    ->join('supplier','supplier.id', '=', 'produk.id_supplier')
                    ->join('variant','variant.id','=','produk.id_variant')
                    ->select('supplier.nama_supplier','variant.*','produk.*')
                    ->where('produk.id', $id_produk)
                    ->first();
        $riwayat = DB::table('riwayat_stok')
                    ->where('id_produk', $id_produk)
                    ->orderby('created_at', 'desc')
                    ->get();

        $supplier = DB::table('supplier')->get();
        $variant=DB::table('variant')->get();
        return view('admin.produk.detail', ['produk' => $produk, 'riwayat' => $riwayat, 'supplier' => $supplier,'variant'=>$variant]);
    }

    public function add()
    {
        $supplier = DB::table('supplier')->get();
        $variant = DB::table('variant')->get();
        return view('admin.produk.add', ['supplier' => $supplier,'variant'=>$variant]);
    }

    public function store(Request $request)
    {
        $produk = DB::table('produk');
        $produk->insert($request->except(['_token']));
        return redirect()->route('produk')->with('sukses', 'Berhasil menambah produk');
    }

    public function update(Request $request, $id)
    {
        $id_produk = Crypt::decrypt($id);
        DB::table('produk')
        ->where('id', $id_produk)
        ->update($request->except(['_token','_method']));
        return redirect()->back()->with('sukses', 'Berhasil mengubah produk');
    }

    public function add_stok(Request $request)
    {
        $stokNow = $request->stok + $request->prevStok;
        DB::table('produk')
        ->where('id', $request->id_produk)
        ->update([
            'stok' => $stokNow,
        ]);
        
        DB::table('riwayat_stok')->insert([
            'id_produk' => $request->id_produk,
            'id_user' => Auth::user()->id,
            'stok' => $request->stok,
            'hasil' => $stokNow,
        ]);

        return redirect()->back()->with('sukses', 'Berhasil menambah stok');
    }

    public function cancel_stok(Request $request)
    {
        DB::table('produk')
        ->where('id', $request->id_produk)
        ->update([
            'stok' => $request->stok - $request->prevStok,
        ]);
        DB::table('riwayat_stok')->where('id', $request->id_stok)->delete();

        return redirect()->back()->with('sukses', 'Berhasil membatalkan stok');
    }

    public function delete($id)
    {
       $id_produk = Crypt::decrypt($id);
       DB::table('riwayat_stok')
       ->where('id_produk', $id_produk)
       ->delete();
        
       $detail = DB::table('detail_transaksi')->where('id_produk', $id_produk);
       $detail_id = $detail->get();
       $detail->delete();
       foreach($detail_id as $det){
           DB::table('transaksi')->where('id', $det->id_transaksi)->delete();
       }

       DB::table('produk')
       ->where('id','=', $id_produk)
       ->delete();
       return redirect()->route('produk')->with('sukses', 'Berhasil Menghapus produk');
    }
}
