<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Crypt;

class supplierController extends Controller
{
    public function index(Request $request)
    {
        $key = $request->get('key');
        $db = DB::table('supplier'); 
        if($key !== null){
            $db->orWhere('nama_supplier','like','%'.$key.'%');
        }
        $supplier = $db->orderby('created_at', 'desc')->get();
        return view('admin.supplier.index', ['supplier' => $supplier]);
    }

    public function detail($id)
    {
        $supplier_id = Crypt::decrypt($id);
        $supplier = DB::table('supplier')->where('id','=', $supplier_id)->first(); 
        return view('admin.supplier.index', ['supplier' => $supplier]);
    }

    public function store(Request $request)
    {
        DB::table('supplier')->insert($request->except(['_token']));
        return redirect()->route('supplier')->with('sukses', 'Berhasil menambah supplier');
    }

    public function update(Request $request, $id)
    {
        $supplier_id = Crypt::decrypt($id);
        DB::table('supplier')
        ->where('id','=',$supplier_id)
        ->update($request->except(['_token','_method']));
        return redirect()->back()->with('sukses', 'Berhasil mengubah supplier');
    }

    public function delete($id)
    {
       $produk = DB::table('produk')
                ->where('id_supplier','=', Crypt::decrypt($id));
        foreach ($produk->get() as $pro) {
            DB::table('riwayat_stok')
            ->where('id_produk', $pro->id)
            ->delete();
            $detail = DB::table('detail_transaksi')->where('id_produk', $pro->id);
            $detail_id = $detail->get();
            $detail->delete();
            foreach($detail_id as $det){
                DB::table('transaksi')->where('id', $det->id_transaksi)->delete();
            }
        }
        $produk->delete();

        DB::table('supplier')
       ->where('id','=', Crypt::decrypt($id))
       ->delete();

       return redirect()->route('supplier')->with('sukses', 'Berhasil Menghapus supplier');
    }

}
