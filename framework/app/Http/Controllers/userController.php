<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{
    public function dashboard()
    {
        if(Auth::user()->role == 1){
            return redirect()->route('penjualan');
        }

        $produk = DB::table('produk')
            ->join('variant', 'produk.id_variant', '=', 'variant.id')
            ->select('variant.*', 'produk.*')
            ->orderby('stok', 'asc')->get();
        $data['jml_produk'] = DB::table('produk')->count();
        $data['jml_pembeli'] = DB::table('pembeli')->count();
        $data['jml_supplier'] = DB::table('supplier')->count();
        $data['jml_operator'] = DB::table('users')->where('role', 1)->count();
        $data['jml_transaksi'] = DB::table('transaksi')->count();
        $penjualan = DB::table('transaksi')->sum('jumlah_total');
        $pokok = DB::table('transaksi')->sum('jumlah_pokok');
        $data['jml_penjualan'] = $penjualan;
        // $ongkir=DB::table('transaksi')->sum('ongkir');
        // $admin=DB::table('transaksi')->sum('admin');
        $data['jml_laba'] = $penjualan - $pokok;
        $data['kotor'] = $pokok;
        $jml = [];
        for ($i = 0; $i < 7; $i++) {
            $between = [
                Carbon::now()->addDay(-$i)->format('Y-m-d 00:00:00'),
                Carbon::now()->addDay(-$i)->format('Y-m-d 23:59:59')
            ];
            $jml[$i] = [
                'jml' => DB::table('transaksi')
                    ->whereBetween('created_at', $between)
                    ->count(),

                'tgl' =>  Carbon::now()->addDay(-$i)->format('Y-m-d 00:00:00'),
            ];
        }

        // User paling sering beli
        $get_user_tersering_beli = DB::table('transaksi')
            ->groupBy('transaksi.id_pembeli')
            ->select('*', DB::raw('count(*) as total'))
            ->join('pembeli', 'pembeli.id', '=', 'transaksi.id_pembeli')
            ->orderBy('total', 'desc')
            ->get();
        // Produk terlaris
        $get_produk_terlaris = DB::table('detail_transaksi')
            ->groupBy('detail_transaksi.id_produk')
            ->select('*', DB::raw('count(*) as total'))
            ->join('produk', 'produk.id', '=', 'detail_transaksi.id_produk')
            ->join('variant', 'produk.id_variant', '=', 'variant.id')
            ->orderBy('total', 'desc')
            ->get();
        $get_order = DB::table('transaksi')->where('id_stat', 1)->count();
        $get_proses = DB::table('transaksi')->where('id_stat', 2)->count();
        $get_dikirim = DB::table('transaksi')->where('id_stat', 3)->count();
        $get_terkirim = DB::table('transaksi')->where('id_stat', 4)->count();
        $get_dikembalikan = DB::table('transaksi')->where('id_stat', 5)->count();

        $data['user_sering_beli'] = $get_user_tersering_beli;
        $data['produk_terlaris'] = $get_produk_terlaris;
        $data['order'] = $get_order;
        $data['proses'] = $get_proses;
        $data['dikirim'] = $get_dikirim;
        $data['terkirim'] = $get_terkirim;
        $data['dikembalikan'] = $get_dikembalikan;

        if(Auth::user()->role != 1){
            return view('dashboard', ['produk' => $produk, 'data' => $data, 'jml' => $jml]);
        }
    }

    public function changePassword(Request $req)
    {
        $id = Auth::user()->id;
        $usr = DB::table('users')->where('id', $id)->first();
        $hash_password_saya = Hash::make($req->confirm_password);
        if (Hash::check($req->old_password, $usr->password)) {
            if (strlen($req->confirm_password) < 8) {
                return redirect()->back()->with('error', 'password min 8 karakter');
            }
            if ($req->new_password !== $req->confirm_password) {
                return redirect()->back()->with('error', 'confirmasi password salah');
            }
            DB::table('users')
                ->where('id', $id)
                ->update(['password' => $hash_password_saya]);
            return redirect()->back()->with('sukses', 'password Berhasil di ubah');
        } else {
            return redirect()->back()->with('error', 'password lama salah');
        }
    }

    public function cekongkir(Request $request)
    {
        $id = $request->get('id');
        $ongkir = null;
        $expedisi = null;
        if ($id !== null) {
            $cek = DB::table('cek_ongkir')->where('id', $id)->first();
            $ongkir = $cek->harga;
            $expedisi = $cek->expedisi;
        }
        $jnt = DB::table('cek_ongkir')->where('expedisi', 'J&T Express')->get();
        $ninja = DB::table('cek_ongkir')->where('expedisi', 'Ninja Express')->get();
        return view('cek_ongkir', ['jnt' => $jnt, 'ninja' => $ninja, 'ongkir' => $ongkir, 'expedisi' => $expedisi]);
    }

    public function laporan(Request $request)
    {
        $range = $request->get('range');
        if ($range !== null) {
            $arr = explode(" - ", $range);

            $data = DB::table('transaksi')
                ->join('pembeli', 'pembeli.id', '=', 'transaksi.id_pembeli')
                ->join('stat', 'stat.id', '=', 'transaksi.id_stat')
                ->join('users', 'users.id', '=', 'transaksi.id_user')
                ->join('method_pem', 'method_pem.id', '=', 'transaksi.id_method')
                ->select('transaksi.*', 'stat.nama_stat', 'users.name', 'method_pem.nama_method_pem', 'pembeli.nama_pembeli', 'pembeli.nomor_hp', 'pembeli.alamat', 'pembeli.kode_pos')
                ->whereBetween('transaksi.created_at', $arr)
                ->get();
                // ->paginate(10);
            // $data->setPath('laporan?range='. $arr[0].'+-+'.$arr[1]);
            foreach ($data as $key => $dt) {
                $produk = DB::table('detail_transaksi')
                    ->join('produk', 'detail_transaksi.id_produk', 'produk.id')
                    ->join('variant', 'variant.id', '=', 'produk.id_variant')
                    ->where('id_transaksi', $dt->id)
                    ->orderby('detail_transaksi.id', 'asc')
                    ->get();
                $data[$key]->listProduk = $produk;
            }

            return view('preview', ['data' => $data, 'arr' => $arr]);
        }

        return view('laporan');
    }
}
