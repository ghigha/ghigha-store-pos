<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Crypt;
use Auth;

class variantController extends Controller
{
    public function index(Request $request)
    {
        $key = $request->get('key');
        $db = DB::table('variant'); 
        if($key !== null){
            $db->orWhere('nama_variant','like','%'.$key.'%');
            $db->orWhere('warna','like','%'.$key.'%');
            $db->orWhere('size','like','%'.$key.'%');
        }
        $supplier = $db->orderby('created_at', 'desc')->get();
        return view('admin.variant.index', ['supplier' => $supplier]);
    }

    public function detail($id)
    {
        $supplier_id = Crypt::decrypt($id);
        $supplier = DB::table('variant')->where('id','=', $supplier_id)->first(); 
        return view('admin.variant.index', ['supplier' => $supplier]);
    }

    public function store(Request $request)
    {
        DB::table('variant')->insert($request->except(['_token']));
        return redirect()->route('variant')->with('sukses', 'Berhasil menambah variant');
    }

    public function update(Request $request, $id)
    {
        $supplier_id = Crypt::decrypt($id);
        DB::table('variant')
        ->where('id','=',$supplier_id)
        ->update($request->except(['_token','_method']));
        return redirect()->back()->with('sukses', 'Berhasil mengubah variant');
    }

    public function delete($id)
    {
       $produk = DB::table('produk')
                ->where('id_variant','=', Crypt::decrypt($id));
        foreach ($produk->get() as $pro) {
            DB::table('riwayat_stok')
            ->where('id_produk', $pro->id)
            ->delete();
            $detail = DB::table('detail_transaksi')->where('id_produk', $pro->id);
            $detail_id = $detail->get();
            $detail->delete();
            foreach($detail_id as $det){
                DB::table('transaksi')->where('id', $det->id_transaksi)->delete();
            }
        }
        $produk->delete();

        DB::table('variant')
       ->where('id','=', Crypt::decrypt($id))
       ->delete();

       return redirect()->route('variant')->with('sukses', 'Berhasil Menghapus variant');
    }
}
