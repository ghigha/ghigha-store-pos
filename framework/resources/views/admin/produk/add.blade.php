@extends('app.app')
@section('content')
 <div class="row justify-content-center">
   <div class="col-lg-10">
     <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="{{url()->previous() }}"></a>Tambah Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('produk.store')}}" method="post">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row justify-content-end">
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Supplier</label> 
                            <small><i class="text-danger">{{($errors->has('id_supplier'))?$errors->first('id_supplier') : ""}}</i></small>
                            <select name="id_supplier" class="form-control">
                                <option value="" selected disabled>-Pilih Supplier-</option>
                                @foreach($supplier as $sup)
                                <option value="{{$sup->id}}">{{$sup->nama_supplier}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label >variant</label> 
                            <small><i class="text-danger">{{($errors->has('id_variant'))?$errors->first('id_variant') : ""}}</i></small>
                            <select name="id_variant" class="form-control">
                                <option value="" selected disabled>-Pilih variant-</option>
                                @foreach($variant as $sup)
                                <option value="{{$sup->id}}">{{$sup->nama_variant}} - {{$sup->warna}} - {{$sup->size}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Nama Produk</label> <small><i class="text-danger">{{($errors->has('nama_produk'))?$errors->first('nama_produk') : ""}}</i></small>
                            <input type="text" name="nama_produk" class="form-control" value="{{old('nama_produk')}}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label >Harga</label> 
                            <small><i class="text-danger">{{($errors->has('harga'))?$errors->first('harga') : ""}}</i></small>
                            <input type="number" name="harga" class="form-control" value="{{old('harga')}}">
                        </div>
                        <div class="form-group">
                            <label >Harga jual</label> 
                            <small><i class="text-danger">{{($errors->has('harga_beli'))?$errors->first('harga_beli') : ""}}</i></small>
                            <input type="number" name="harga_beli" class="form-control" value="{{old('harga_beli')}}">
                        </div>
                        <div class="form-group">
                            <label >Stok awal</label> 
                            <small><i class="text-danger">{{($errors->has('stok'))?$errors->first('stok') : ""}}</i></small>
                            <input type="number" name="stok" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
   </div>
 </div>
@endsection