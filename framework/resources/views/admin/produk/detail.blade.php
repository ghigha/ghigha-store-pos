@extends('app.app')
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="{{url()->previous() }}"></a>Detail {{$produk->nama_produk}}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row justify-content-center">
            <div class="col-11">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <small>Detail</small>
                    <a class="badge badge-danger float-right" href="" data-toggle="modal" data-target="#hapus-produk">
                        <i class="fas fa-trash mr-1"></i>Hapus
                    </a>
                    <a class="badge badge-warning float-right mr-1" href="" data-toggle="modal" data-target="#ubah-produk">
                        <i class="fas fa-edit mr-1"></i>Ubah
                    </a>
                    <hr class="mt-0">
                   <label style="min-width:100px;">Nama Produk </label><span>: {{$produk->nama_produk}}</span>
                   <br>
                   <label style="min-width:100px;">Harga </label><span>: {{$produk->harga}}</span>
                   <br>
                   <label style="min-width:100px;">Harga Jual</label><span>: {{$produk->harga_beli}}</span>
                   <br>
                   <label style="min-width:100px;">variant</label><span>: {{$produk->nama_variant}}</span>
                   <br>
                   <label style="min-width:100px;">warna</label><span>: {{$produk->warna}}</span>
                   <br>
                   <label style="min-width:100px;">size</label><span>: {{$produk->size}}</span>
                   <br>
                   <label style="min-width:100px;">Stok </label><span>: {{$produk->stok}}</span>
                   <a class="badge badge-primary ml-1" href="" data-toggle="modal" data-target="#tambah-stok">
                        <i class="fas fa-plus"></i>
                    </a>
                   <br>
                   <label style="min-width:100px;">Supplier </label><span>: {{$produk->nama_supplier}}</span>
                   <br>
                   <small>Ditambahkan pada {{Carbon\Carbon::parse($produk->created_at)->format('d-m-Y')}}</small>
                   <br>
                   <small>Dirubah pada {{Carbon\Carbon::parse($produk->updated_at)->format('d-m-Y')}}</small>
                </div>
                <div class="col-lg-8 mt-lg-0 mt-4">
                    <small>Riwayat stok</small>
                    <hr class="mt-0">
                    <div class="timeline">
                        @foreach($riwayat as $key => $riw)
                        <div>
                            <i class="fas fa-box bg-info"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fas fa-clock"></i> {{Carbon\Carbon::parse($riw->created_at)->format('d-m-Y')}}</span>
                                <h3 class="timeline-header no-border">
                                    Stok <a class="text-info">{{$produk->nama_produk}}</a>   
                                    ditambah sebanyak : <strong>{{$riw->stok}}</strong>
                                    <br> 
                                    Total stok {{$riw->hasil}}
                                    <br>
                                    @if($key === 0)
                                    <form action="{{route('produk.cancel_stok')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id_stok" value="{{$riw->id}}">
                                        <input type="hidden" name="id_produk" value="{{$produk->id}}">
                                        <input type="hidden" name="prevStok" value="{{$riw->stok}}">
                                        <input type="hidden" name="stok" value="{{$produk->stok}}">
                                        <button type="submit" class="badge badge-info mt-1" style="border: none">
                                            <i class="fas fa-sync-alt mr-1"></i>Batalkan
                                        </button>
                                    </form>
                                    @endif
                                </h3>
                            </div>
                        </div>
                        @endforeach
                        <div>
                            <i class="fas fa-clock bg-gray"></i>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
  <div class="modal fade" id="hapus-produk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Yakin ingin menghapus ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('produk.delete', Crypt::encrypt($produk->id))}}" method="POST">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Yakin</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
  <div class="modal fade" id="ubah-produk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('produk.update', Crypt::encrypt($produk->id))}}" method="POST">
                <div class="modal-body">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <small>Nama</small>
                    <input type="text" class="form-control" name="nama_produk" value="{{$produk->nama_produk}}" required>
                    <small>Harga</small>
                    <input type="number" class="form-control" name="harga" value="{{$produk->harga}}" required>
                    <small>Harga jual</small>
                    <input type="number" class="form-control" name="harga_beli" value="{{$produk->harga_beli}}" required>
                    <small>Supplier</small>
                    <select name="id_supplier" class="form-control" required>
                        <option value="" selected disabled>-Pilih Supplier-</option>
                        @foreach($supplier as $sup)
                        <option value="{{$sup->id}}" {{$sup->id === $produk->id_supplier? "selected" : "" }}>{{$sup->nama_supplier}}</option>
                        @endforeach
                    </select>
                    <small>Supplier</small>
                    <select name="id_variant" class="form-control" required>
                        <option value="" selected disabled>-Pilih variant-</option>
                        @foreach($variant as $sup)
                        <option value="{{$sup->id}}" {{$sup->id === $produk->id_variant? "selected" : "" }}>{{$sup->nama_variant}} - {{$sup->warna}} - {{$sup->size}}</option>
                        @endforeach
                    </select>
                </div>
                {{csrf_field()}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

  <!-- /.card -->
  <div class="modal fade" id="tambah-stok">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Stok</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('produk.add_stok')}}" method="POST">
                <div class="modal-body">
                    {{csrf_field()}}
                    <small>Jumlah Stok</small>
                    <input type="text" class="form-control" name="stok" required>
                    <input type="hidden" class="form-control" name="id_produk" value="{{$produk->id}}">
                    <input type="hidden" class="form-control" name="prevStok" value="{{$produk->stok}}">
                </div>
                {{csrf_field()}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection