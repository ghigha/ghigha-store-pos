@extends('app.app')
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><i class="fas fa-user-tie mr-1"></i>Detail Nama Supplier</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <form action="{{route('supplier')}}">
                      <div class="input-group">
                    <div class="custom-file">
                      <input type="text" name="key" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>
                </div>
                <div class="col-lg-8">
                  <a class="btn btn-primary float-lg-right mr-3 mt-3 mt-lg-0 text-light">
                  <i class="fas fa-plus mr-1"></i>Tambah Supplier</a>
                </div>
              </div>
              <small><i class="fas fa-list"></i> Daftar supplier</small>
              <hr class="mt-0">
              <div class="row">
                <div class="col-lg-2 col-6">
                    <div class="card text-center">
                        <div class="card-body">
                            <a href="" class="d-block mb-4" style="line-height:15px;">
                                <label class="d-block">Nama Suplier </label>
                                <small class="d-block">2 produk</small>
                            </a>
                            <a class="badge badge-warning" href=""><i class="fas fa-edit mr-1"></i>Ubah</a>
                            <a class="badge badge-danger" data-toggle="modal" data-target="#delete" href=""><i class="fas fa-trash mr-1"></i>Hapus</a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
@endsection