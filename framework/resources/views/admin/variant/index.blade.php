@extends('app.app')
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><i class="fas fa-user-tie mr-1"></i>Kelola variant</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row">
            <div class="col-12">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <form action="{{route('variant')}}">
                      <div class="input-group">
                    <div class="custom-file">
                      <input type="text" name="key" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>
                </div>
                <div class="col-lg-8">
                  <a data-toggle="modal" data-target="#tambah-supplier" class="btn btn-primary float-lg-right mr-3 mt-3 mt-lg-0 text-light">
                  <i class="fas fa-plus mr-1"></i>Tambah variant</a>
                </div>
              </div>
              <small><i class="fas fa-list"></i> Daftar variant</small>
              <hr class="mt-0">
              <div class="row">
                @foreach($supplier as $sup)
                <div class="col-lg-2 col-6">
                    <div class="card text-center" style="height:90%">
                        <div class="card-body">
                            <a href="" class="d-block mb-2" style="line-height:19px;">
                                <label class="d-block" style="min-height:20px;">{{Str::limit($sup->nama_variant, 23)}}</label>
                                <label class="d-block" style="min-height:20px;">warna : {{Str::limit($sup->warna, 23)}}</label>
                                <label class="d-block" style="min-height:20px;">size : {{Str::limit($sup->size, 23)}}</label>
                                <small class="d-block">dibuat {{Carbon\Carbon::parse($sup->created_at)->format('d-m-Y')}}</small>
                            </a>
                            <a class="badge badge-warning" href="" data-toggle="modal" data-target="#ubah-supplier{{$sup->id}}"><i class="fas fa-edit mr-1"></i>Ubah</a>
                            <a class="badge badge-danger" data-toggle="modal" data-target="#delete-supplier{{$sup->id}}" href=""><i class="fas fa-trash mr-1"></i>Hapus</a>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="delete-supplier{{$sup->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Yakin ingin menghapus ?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                              <label for="">Peringatan</label><br>
                              <span>Menghapus variant akan otomatis menghapus data produk yang terhubung dengan variant.</span>
                            </div>
                            <form action="{{route('variant.delete', Crypt::encrypt($sup->id))}}" method="POST">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Yakin</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="ubah-supplier{{$sup->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Ubah variant</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{route('variant.update', Crypt::encrypt($sup->id))}}" method="POST">
                                <div class="modal-body">
                                    {{csrf_field()}}
                                    {{method_field('PUT')}}
                                    <small>Nama variant</small>
                                    <input type="text" class="form-control" name="nama_variant" value="{{$sup->nama_variant}}">
                                    <small>warna</small>
                                    <input type="text" class="form-control" name="warna" value="{{$sup->warna}}">
                                    <small>size</small>
                                    <input type="text" class="form-control" name="size" value="{{$sup->size}}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Ubah</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                @endforeach
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->

<div class="modal fade" id="tambah-supplier">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah variant</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('variant.store')}}" method="POST">
                <div class="modal-body">
                    {{csrf_field()}}
                    <small>Nama variant</small>
                    <input type="text" class="form-control" name="nama_variant" value="{{old('nama_variant')}}">
                    <small>warna</small>
                    <input type="text" class="form-control" name="warna" value="{{old('warna')}}">
                    <small>size</small>
                    <input type="text" class="form-control" name="size" value="{{old('size')}}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection