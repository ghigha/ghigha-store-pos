@extends('app.app')
@section('content')
 <div class="row justify-content-center">
   <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="nav-icon fas fa-box mr-2"></i> Cek Ongkir</h3>
            </div>
            <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <small>Cek ongkir J&T Express</small>
                                <form action="{{route('cekongkir')}}">
                                    <div class="row">
                                        <div class="col-5">
                                            <select name="asal" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Asal-</option>
                                                <option value="Jakarta">Jakarta</option>
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <select name="id" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Tujuan-</option>
                                                @foreach($jnt as $pem)
                                                <option value="{{$pem->id}}">{{$pem->tujuan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-2">
                                        <button type="submit" class="btn btn-primary">Cek</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <small>Cek ongkir Ninja Express</small>
                                <form action="{{route('cekongkir')}}">
                                    <div class="row">
                                        <div class="col-5">
                                            <select name="asal" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Asal-</option>
                                                <option value="Jakarta">Jakarta</option>
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <select name="id" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Tujuan-</option>
                                                @foreach($ninja as $pem)
                                                <option value="{{$pem->id}}">{{$pem->tujuan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-2">
                                        <button type="submit" class="btn btn-primary">Cek</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-4 mt-5">
                                <small>Ongkir</small>
                                <input type="text" class="form-control" value="{{$ongkir}}">
                            </div>
                            <div class="col-2 mt-5">
                            <form action="{{route('penjualan.add')}}">
                                <input type="hidden" name="ongkir" class="form-control" value="{{$ongkir}}">
                                <input type="hidden" name="expedisi" class="form-control" value="{{$expedisi}}">
                            <br>
                                <button type="submit" class="btn btn-primary">Tambahkan</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
 </div>
@endsection