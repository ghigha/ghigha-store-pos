@extends('app.app')
@section('content')
 <div class="row justify-content-center">
   <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">  <i class="nav-icon fas fa-tachometer-alt mr-2"></i> Dashboard</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Produk</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['jml_produk']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Supplier</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['jml_supplier']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Pembeli</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['jml_pembeli']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Operator</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['jml_operator']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Transaksi</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['jml_transaksi']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Penjualan</label>
                                    <input type="hidden" value="{{$data['jml_penjualan']}}" id="jmlp">
                                    <span style="font-size:20px;display:block;margin-top:5px;margin-bottom:12px;" id="penjualan" ></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Modal</label>
                                    <input type="hidden" value="{{$data['kotor']}}" id="ikotor">
                                    <span style="font-size:20px;display:block;margin-top:5px;margin-bottom:12px;" id="kotor"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Laba Lersih</label>
                                    <input type="hidden" value="{{$data['jml_laba']}}" id="jmll">
                                    <span style="font-size:20px;display:block;margin-top:5px;margin-bottom:12px;" id="laba"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <small><b>Jumlah Penjualan 7 Hari Terakhir</b></small>
                    <?php $d = '[' ?>
                    <?php $t = "[" ?>
                    <?php $max = 0 ?>
                    @for($x = 6; $x >= 0; $x--)
                        <?php  $d .= $jml[$x]['jml'].','?>
                        @if($x===0)
                        <?php $tmp = "Hari ini"; ?>
                        @else
                        <?php $tmp = \Carbon\Carbon::parse($jml[$x]['tgl'])->format('d-M'); ?>
                        @endif
                        <?php  $t .="'". $tmp ."',"?>
                        @if($max <= $jml[$x]['jml'] + 9)
                            <?php $max = $jml[$x]['jml'] + 9 ?>
                        @endif
                    @endfor
                    <?php $d .= ']' ?>
                    <?php $t .= "]" ?>
                      <div class="chart">
                        <canvas id="barChart" style="height:350px; min-height:230px"></canvas>
                    </div> 
                    </div>
                    <div class="col-lg-4">
                    <small><b>Produk Hampir Habis</b></small>
                        <div class="bg-light p-2" style="height:450px;overflow-x: hidden; 
                    overflow-y: auto; 
                    text-align:justify;">
                            <div class="row">
                                @foreach($produk as $pro)
                                <div class="col-12">
                                    <div class="card ">
                                        <div class="card-body text-center p-2">
                                            <a style="display:block;min-height:50px" class="text-dark" href="{{route('produk.detail', Crypt::encrypt($pro->id))}}">
                                                {{$pro->nama_produk}}
                                            </a> 
                                            <small>
                                                {{$pro->nama_variant}}</br>
                                               warna : {{$pro->warna}}<br>
                                               size : {{$pro->size}}<br>
                                            </small> 
                                            <label>{{$pro->stok}} tersedia</label>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
 </div>
<!-- Status -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <small><b>Status Pembelian</b></small>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-6">
                        <div class="card ">
                            <div class="card-body text-center p-2">
                                <label>Order</label>
                                <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['order']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="card ">
                            <div class="card-body text-center p-2">
                                <label>Proses</label>
                                <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['proses']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="card ">
                            <div class="card-body text-center p-2">
                                <label>Dikirim</label>
                                <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['dikirim']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-6">
                        <div class="card ">
                            <div class="card-body text-center p-2">
                                <label>Terkirim</label>
                                <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['terkirim']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-6">
                        <div class="card ">
                            <div class="card-body text-center p-2">
                                <label>Dikembalikan</label>
                                <span style="font-size:40px;display:block;margin-top:-15px;">{{$data['dikembalikan']}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- Status -->
 <div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <small><b>Customer Loyal </b></small>
                </div>
                <br>
                <hr>
                <div class="bg-light p-3" style="height:450px;overflow-x: hidden; 
                    overflow-y: auto; 
                    text-align:justify;">
                    <div class="row">
                        @foreach($data['user_sering_beli'] as $val)
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            {{ $val->nama_pembeli }} <a class="badge badge-primary ml-2" href="{{route('pembeli.detail', Crypt::encrypt($val->id))}}"><i class="fas fa-eye mr-1"></i>detail</a>
                                        </div>
                                        <div class="col-sm-2">
                                            :
                                        </div>
                                        <div class="col-sm-2">
                                            ({{ $val->total }})
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Produk sering di beli -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <small><b>Produk Terlaris </b></small>
                </div>
                <br>
                <hr>
                <div class="bg-light p-3" style="height:450px;overflow-x: hidden; 
                    overflow-y: auto; 
                    text-align:justify;">
                    <div class="row">
                        @foreach($data['produk_terlaris'] as $val)
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            {{ $val->nama_produk }} 
                                            <a href="{{route('produk.detail', Crypt::encrypt($val->id_produk))}}" class="badge badge-primary ml-2" style="line-height:19px;">
                                                <i class="fas fa-eye mr-1"></i>detail
                                            </a>
                                            <br>
                                            <small>{{ $val->nama_variant }}</small>
                                        </div>
                                        <div class="col-sm-4 text-right">
                                            ({{ $val->total }})
                                        </div>
                                        <!-- variant -->
                                        <div class="col-6 mt-2">
                                            Warna : <small>{{ $val->warna }}</small>
                                        </div>
                                        <div class="col-6 mt-2 text-right">
                                            Size : <small>{{ $val->size }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 @push('js')
 <script>
function toRupiah(angka){
            var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return rupiah;
        }

        var penjualan=$('#jmlp').val();
        var laba=$('#jmll').val()
        var kot=$('#ikotor').val()
        $('#penjualan').text('Rp. '+toRupiah(penjualan));
        $('#laba').text('Rp. '+toRupiah(laba))
        $('#kotor').text('Rp. '+toRupiah(kot))


 var ctx = document.getElementById("barChart");
var myBarChart = new Chart(ctx, {
type: 'bar',
data: {
    labels  : <?= $t ?>,
    datasets: [
    {
        label               : 'Penjualan',
        backgroundColor     : 'rgba(60,141,188,0.9)',
        borderColor         : 'rgba(60,141,188,0.8)',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : {{$d}},
    }
    ],
},
options: {
    maintainAspectRatio: false,
    layout: {
    padding: {
        bottom: 0
    }
    },
    scales: {
    xAxes: [{
        time: {
        unit: 'month'
        },
        gridLines: {
        display: false,
        drawBorder: false
        },
        ticks: {
        maxTicksLimit: 6
        },
        maxBarThickness: 200,
    }],
    yAxes: [{
        ticks: {
        min: 0,
        max: {{$max}},
        padding: 10,
        // Include a dollar sign in the ticks
        callback: function(value, index, values) {
            return value;
        }
        },
        gridLines: {
        color: "rgb(234, 236, 244)",
        zeroLineColor: "rgb(234, 236, 244)",
        drawBorder: false,
        borderDash: [2],
        zeroLineBorderDash: [2]
        }
    }],
    },
    legend: {
    display: false
    },
    tooltips: {
    titleMarginBottom: 10,
    titleFontColor: '#6e707e',
    titleFontSize: 14,
    backgroundColor: "rgb(255,255,255)",
    bodyFontColor: "#858796",
    borderColor: '#dddfeb',
    borderWidth: 1,
    xPadding: 15,
    yPadding: 15,
    displayColors: false,
    caretPadding: 10,
    callbacks: {
        label: function(tooltipItem, chart) {
        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
        return datasetLabel + ': ' + tooltipItem.yLabel + ' kali';
        }
    }
    },
}
});

 </script>
 @endpush
@endsection