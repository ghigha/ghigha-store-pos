@extends('app.app')
@section('content')
 <div class="row justify-content-center">
   <div class="col-lg-10">
     <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Pembeli</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('pembeli.store')}}" method="post">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row justify-content-end">
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Nama</label> <small><i class="text-danger">{{($errors->has('nama_pembeli'))?$errors->first('nama_pembeli') : ""}}</i></small>
                            <input type="text" name="nama_pembeli" class="form-control" value="{{old('nama_pembeli')}}" required>
                        </div>
                        <div class="form-group">
                            <label >Nomor Hp</label> <small><i class="text-danger">{{($errors->has('nomor_hp'))?$errors->first('nomor_hp') : ""}}</i></small>
                            <div class="input-group">
                                <div class="input-group-prepend pr-2">
                                    <span>+62</span>
                                </div>
                                <input type="text" name="nomor_hp" class="form-control" value="{{old('nomor_hp')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Provinsi</label> <small><i class="text-danger">{{($errors->has('provinsi'))?$errors->first('provinsi') : ""}}</i></small>
                            <input type="text" name="provinsi" class="form-control" value="{{old('provinsi')}}">
                        </div>
                        <div class="form-group">
                            <label >Kabupaten</label> <small><i class="text-danger">{{($errors->has('kabupaten'))?$errors->first('kabupaten') : ""}}</i></small>
                            <input type="text" name="kabupaten" class="form-control" value="{{old('kabupaten')}}">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Alamat lengkap</label> 
                            <small><i class="text-danger">{{($errors->has('alamat'))?$errors->first('alamat') : ""}}</i></small>
                            <textarea name="alamat" class="form-control" cols="30" rows="8">{{old('alamat')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label >Kode pos</label> 
                            <small><i class="text-danger">{{($errors->has('kode_pos'))?$errors->first('kode_pos') : ""}}</i></small>
                            <input type="number" name="kode_pos" class="form-control w-25">
                        </div>
                        <input type="hidden" name="id_user" class="form-control w-25" value="{{Auth::user()->id}}">
                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
   </div>
 </div>
@endsection