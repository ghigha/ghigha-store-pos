@extends('app.app')
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <form action="{{route('pembeli')}}">
                      <div class="input-group">
                    <div class="custom-file">
                      <input type="text" name="key" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>
                </div>
                <div class="col-lg-8">
                  <a href="{{route('pembeli.add')}}" class="btn btn-primary float-lg-right mr-3 mt-3 mt-lg-0">
                  <i class="fas fa-plus mr-1"></i>Tambah pembeli</a>
                </div>
              </div>
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama pembeli</th>
                  <th>Alamat</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach($pembeli as $pem)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$pem->nama_pembeli}}</td>
                  <td>{{$pem->alamat}}</td>
                  <td>
                    <a class="badge badge-primary" href="{{route('pembeli.detail', Crypt::encrypt($pem->id))}}"><i class="fas fa-eye mr-1"></i>detail</a>
                    <a class="badge badge-danger" data-toggle="modal" data-target="#delete{{$pem->id}}" href=""><i class="fas fa-trash mr-1"></i>Hapus</a>
                  </td>
                  <div class="modal fade" id="delete{{$pem->id}}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Yakin Ingin Menghapus ?</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="{{route('pembeli.delete', Crypt::encrypt($pem->id))}}" method="POST">
                          {{csrf_field()}}
                          {{method_field('DELETE')}}
                          <div class="modal-body">
                              <label for="">Peringatan</label><br>
                              <span>Menghapus pembeli akan otomatis menghapus data penjualan yang terhubung dengan pembeli.</span>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary">Yakin</button>
                        </div>
                         </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
@endsection