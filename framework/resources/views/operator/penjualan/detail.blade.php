@extends('app.app')
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="{{url()->previous() }}"></a>Detail {{$penjualan->no_pesanan}}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row justify-content-center">
            <div class="col-11">
              <div class="row mb-3">
                <div class="col-lg-12">
                    <small>Detail</small>
                    <a class="badge badge-danger float-right" href="" data-toggle="modal" data-target="#hapus-penjualan">
                        <i class="fas fa-trash mr-1"></i>Hapus
                    </a>
                    <a class="badge badge-warning float-right mr-1" href="{{route('penjualan.edit', Crypt::encrypt($penjualan->id))}}">
                        <i class="fas fa-edit mr-1"></i>Ubah
                    </a>
                    <hr class="mt-0">
                   <label style="min-width:100px;">No Pesanan </label><span>: {{$penjualan->no_pesanan}}</span>
                   <br>
                   <label style="min-width:100px;">No Resi </label><span>: {{$penjualan->no_resi ? $penjualan->no_resi : "Belum dikirim"}}</span>
                   <br>
                   <label style="min-width:100px;">Status </label><span>: {{$penjualan->nama_stat}}</span>
                   <a class="badge badge-warning ml-1" href="" data-toggle="modal" data-target="#ubah-status">
                        <i class="fas fa-edit"></i> Ubah status
                    </a>
                   <br>
                   <label style="min-width:100px;">Pembeli </label><span>: {{$penjualan->nama_pembeli}}</span>
                   <a class="badge badge-primary ml-1" href="{{route('pembeli.detail', Crypt::encrypt($penjualan->id_pembeli))}}" >
                        <i class="fas fa-eye"></i> detail
                    </a>
                    <br>
                   <label style="min-width:100px;">Nomor Hp </label><span>: +62{{$penjualan->nomor_hp}}</span>
                   <a class="badge badge-primary ml-1" target="_blank" href="https://wa.me/62{{$penjualan->nomor_hp}}">
                        <i class="fab fa-whatsapp"></i> chat
                    </a>
                   <br>
                   <label style="min-width:100px;">Pembayaran </label><span>: {{$penjualan->nama_method_pem}}</span>
                   <br>
                   <label style="min-width:100px;">expedisi </label><span>: {{$penjualan->expedisi}}</span>
                   <br>
                    <div>
                        <label class="float-left d-block" style="min-width:100px;height:80px;">Dikirim ke</label>
                        <span class=" d-block" style="min-width:100px;min-height:80px;">: {{$penjualan->alamat}}</span>
                    </div>
                    <br>
                     <label style="min-width:100px;">Kode Pos </label><span>: {{$penjualan->kode_pos}}</span>
                    <br>
                    <br>
                   <small class="d-lg-inline-block d-none">Ditambahkan oleh {{$penjualan->name}} pada {{Carbon\Carbon::parse($penjualan->created_at)->format('d-m-Y H:i')}}</small>
                </div>
                <div class="mt-0 col-lg-12 mt-lg-4">
                    <hr class="mt-0 d-lg-block d-none"> 
                    <small>Barang yang dibeli</small>
                    <div class="table-responsive" style="max-height:350px">
                    <table class="table table-hover" id="table-preview">
                        <tr class="bg-dark text-white">
                            <td>Nama Barang</td>
                            <td>Variant</td>
                            <td style="width:20px">Qty</td>
                            <td class="text-center">Harga</td>
                            <td>Jumlah</td>
                        </tr>
                        @foreach($produk as $pro)
                        <tr>
                            <td>{{$pro->nama_produk}}</td>
                            <td>{{$pro->nama_variant}}</td>
                            <td>{{$pro->qty}}</td>
                            <td class="text-center">Rp{{$pro->harga_beli}}</td>
                            <td>Rp{{$pro->harga_beli * $pro->qty}}</td>
                        </tr>
                        @endforeach
                        <tr class="bg-light">
                            <td colspan="4" class="text-right">Ongkos kirim</td>
                            <td id="jumlah-ongkir">Rp{{$penjualan->ongkir}}</td>
                        </tr>
                        <tr class="bg-light">
                            <td colspan="4" class="text-right">Biaya admin</td>
                            <td id="tampil-admin">Rp{{$penjualan->admin}}</td>
                        </tr>
                        <tr style="border-top:2px solid black" class="bg-dark text-light">
                            <td colspan="4" class="text-right">Jumlah Total</td>
                            <td id="total-jumlah">Rp{{$penjualan->jumlah_total}}</td>
                        </tr>
                    </table>
                </div>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>

 <!-- /.modal -->
 <div class="modal fade" id="ubah-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('penjualan.update_stat', Crypt::encrypt($penjualan->id))}}" method="POST">
                <div class="modal-body">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <small>Status*</small>
                    <div class="input-group mb-2">
                        <select name="id_stat" class="form-control" required>
                            @foreach($stats as $stat)
                            <option value="{{$stat->id}}" {{$penjualan->id_stat === $stat->id ? "selected" : ""}}>{{$stat->nama_stat}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection