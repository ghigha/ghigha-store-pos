@extends('app.app')
@section('content')
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><i class="fas fa-chart-bar mr-1"></i>Kelola penjualan</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <form action="{{route('penjualan')}}">
                      <div class="input-group">
                    <div class="custom-file">
                      <input type="text" name="key" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>
                </div>
                <div class="col-lg-8 text-right">
                  <a href="{{route('pembeli')}}" class="text-dark mr-4"> <i class="fas fa-user mr-1"></i>Kelola pembeli</a>
                  <a href="{{route('penjualan.add')}}" class="btn btn-primary mr-3 mt-3 mt-lg-0">
                  <i class="fas fa-plus mr-1"></i>Tambah penjualan</a>
                </div>
              </div>
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Pesanan</th>
                  <th>Nama pembeli</th>
                  <th>Produk</th>
                  <th>Qty</th>
                  <th>Variant</th>
                  <th>Warna</th>
                  <th>Nomor Hp</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach($penjualan as $pem)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$pem->no_pesanan}}</td>
                  <td>{{$pem->nama_pembeli}}</td>
                  <td>{{$pem->nama_produk}}</td>
                  <td>{{$pem->qty}}</td>
                  <td>{{ $pem->nama_variant }}</td>
                  <td>{{ $pem->warna }}</td>
                  <td>
                    +62{{$pem->nomor_hp}} 
                    <a class="badge badge-primary ml-1" target="_blank" href="https://wa.me/62{{$pem->nomor_hp}}">
                        <i class="fab fa-whatsapp"></i> chat
                    </a>
                  </td>
                  <td>{{$pem->nama_stat}}</td>
                  <td>
                    <a class="badge badge-primary" href="{{route('penjualan.detail', Crypt::encrypt($pem->id_transaksi))}}"><i class="fas fa-eye mr-1"></i>detail</a>
                    <a class="badge badge-danger" data-toggle="modal" data-target="#delete{{$pem->id_transaksi}}" href=""><i class="fas fa-trash mr-1"></i>Hapus</a>
                  </td>
                  <div class="modal fade" id="delete{{$pem->id_transaksi}}">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Yakin Ingin Menghapus ?</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="{{route('penjualan.delete', Crypt::encrypt($pem->id_transaksi))}}" method="POST">
                          {{csrf_field()}}
                          {{method_field('DELETE')}}
                       
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary">Yakin</button>
                        </div>
                         </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
@endsection