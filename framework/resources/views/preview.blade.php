<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan</title>
    <style>
        *{
            padding: 0px;
            margin: 0px;
            font-family: Arial, Helvetica, sans-serif;
        }
        body{
            background-color: rgba(0,0,0,0.1);
            padding: 20px 20px;
            text-align:center;
        }
        .wrapper{
            background-color: white;
            width: 100%;
            padding: 20px 0px;
            text-align: center;
            min-height:550px;
        }
        h3{
            margin-bottom: 20px;
        }
        td{
            padding: 5px ;
        }
        table{
            margin: 0px auto;
        }
        a{
            display:block;
            margin-bottom:20px;
            padding:10px;
            background-color: rgba(0,0,0,0.6);
            color:white;
            text-decoration:none;
        }
        @media print {
         a {display:none}
         .pagination{display: none}
      }
      .pagination{
        padding-top: 2rem;
        padding-bottom: 2rem;
      }
      .pagination ul li{
        width: 100px;
        list-style: none;
        display: inline-block;
      }
      .str{ mso-number-format:\@; }

    </style>
</head>
<body>
    <a href="#" onclick="history.back()" style="margin-bottom:5px">Kembali</a>
    <a onclick="exportToPDF('myDataTable')" href="#">Preview</a>
    <a onclick="exportToExcel('tab-content-excel')" href="#">Export Excel</a>
    <div class="wrapper" id="tab-content">
        <img src="{{url('template')}}/dist/img/logo.png" alt="AdminLTE Logo"
         style="opacity: .8; max-width:50px;"> <h2>Ghigha Store</h2>
        <h3>Rekap penjualan | {{$arr[0]}} s/d {{$arr[1]}}</h3>
        <table border="1" cellspacing="0" id="myDataTable">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">No Pesanan</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">operator</th>
                    <th rowspan="2">Metode</th>
                    <th rowspan="2">Status</th>
                    <th rowspan="2">No Resi</th>
                    <th rowspan="2">Expedisi</th>
                    <th rowspan="2">Ongkir</th>
                    <th rowspan="2">Admin</th>
                    <th colspan="6">Produk</th>
                    <th rowspan="2">Jumlah Total</th>
                </tr>
                <tr>
                    <th>Nama</th>
                    <th>variant</th>
                    <th>Qty</th>
                    <th>warna</th>
                    <th>size</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;?>
                <?php $semua = 0;?>
                @foreach($data as $dt)                
                <?php $length = count($dt->listProduk) ?>
                <tr>
                    <td rowspan="{{ $length }}" align="center">{{ $no++ }}</td>
                    <td rowspan="{{$length}}">{{$dt->no_pesanan}}</td>
                    <td rowspan="{{$length}}">{{$dt->nama_pembeli}}</td>
                    <td rowspan="{{$length}}">{{$dt->name}}</td>
                    <td rowspan="{{$length}}" >{{$dt->nama_method_pem}}</td>
                    <td rowspan="{{$length}}" >{{$dt->nama_stat}}</td>
                    <td rowspan="{{$length}}">{{$dt->no_resi}}</td>
                    <td rowspan="{{$length}}" >{{$dt->expedisi}}</td>
                    <td rowspan="{{$length}}" >{{$dt->ongkir}}</td>
                    <td rowspan="{{$length}}" >{{$dt->admin}}</td>
                    <td >{{$dt->listProduk[0]->nama_produk}}</td>
                    <td >{{$dt->listProduk[0]->nama_variant}}</td>
                    <td >{{$dt->listProduk[0]->qty}}</td>
                    <td >{{$dt->listProduk[0]->warna}}</td>
                    <td >{{$dt->listProduk[0]->size}}</td>
                    <td >{{$dt->listProduk[0]->qty * $dt->listProduk[0]->harga_beli}}</td>
                    <td rowspan="{{$length}}" >Rp{{$dt->jumlah_total}}</td>
                </tr>
                <?php  $semua += $dt->jumlah_total;?>
                @if($length > 1)
                @for($i=1;$i<$length;$i++)
                <tr>
                    <td>{{$dt->listProduk[$i]->nama_produk}}</td>
                    <td>{{$dt->listProduk[$i]->nama_variant}}</td>
                    <td>{{$dt->listProduk[$i]->qty}}</td>
                    <td>{{$dt->listProduk[$i]->warna}}</td>
                    <td>{{$dt->listProduk[$i]->size}}</td>
                    <td>{{$dt->listProduk[$i]->qty * $dt->listProduk[$i]->harga_beli}}</td>
                </tr>
                @endfor
                @endif
                @endforeach
                <tr>
                    <th colspan="12" align="right">Jumlah pendapatan  {{$arr[0]}} s/d {{$arr[1]}}</th>
                    <th colspan="5" align="center">Rp{{$semua}}</th>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="wrapper" id="tab-content-excel" hidden="true">
        <img src="{{url('template')}}/dist/img/logo.png" alt="AdminLTE Logo"
         style="opacity: .8; max-width:50px;"> <h2>Ghigha Store</h2>
        <h3>Rekap penjualan | {{$arr[0]}} s/d {{$arr[1]}}</h3>
        <table border="1" cellspacing="0" id="myDataTable">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">No Pesanan</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">alamat</th>
                    <th rowspan="2">No. HP</th>
                    <th rowspan="2">Metode</th>
                    <th colspan="4">Produk</th>
                    <th rowspan="2">Jumlah Total</th>
                </tr>
                <tr>
                    <th>Nama</th>
                    <th>Qty</th>
                    <th>warna</th>
                    <th>size</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;?>
                <?php $semua = 0;?>
                @foreach($data as $dt)                
                <?php $length = count($dt->listProduk) ?>
                <tr>
                    <td rowspan="{{ $length }}" align="center">{{ $no++ }}</td>
                    <td rowspan="{{$length}}">{{$dt->no_pesanan}}</td>
                    <td rowspan="{{$length}}">{{$dt->nama_pembeli}}</td>
                    <td rowspan="{{$length}}" align="left">{{$dt->alamat}}</td>
                    <td rowspan="{{$length}}" align="center">'+62{{$dt->nomor_hp}}</td>
                    <td rowspan="{{$length}}" >{{$dt->nama_method_pem}}</td>
                    <td >{{$dt->listProduk[0]->nama_produk}}</td>
                    <td >{{$dt->listProduk[0]->qty}}</td>
                    <td >{{$dt->listProduk[0]->warna}}</td>
                    <td >{{$dt->listProduk[0]->size}}</td>
                    <td rowspan="{{$length}}" >Rp{{$dt->jumlah_total}}</td>
                </tr>
                <?php  $semua += $dt->jumlah_total;?>
                @if($length > 1)
                @for($i=1;$i<$length;$i++)
                <tr>
                    <td>{{$dt->listProduk[$i]->nama_produk}}</td>
                    <td>{{$dt->listProduk[$i]->qty}}</td>
                    <td>{{$dt->listProduk[$i]->warna}}</td>
                    <td>{{$dt->listProduk[$i]->size}}</td>
                </tr>
                @endfor
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- <div class="pagination" style="background: white">
        <?php // echo $data->render(); ?>
    </div>
    <input type="hidden" id="page" data-page="@if(isset($_GET['page'])) {{ $_GET['page'] }} @endif"> -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.6/jspdf.plugin.autotable.min.js"></script>  
<script type="text/javascript">
    function exportToExcel(myTableId, filename = '')
    {
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(myTableId);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        
        // Specify file name
        filename = filename?filename+'.xls':'Ghigha.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");
        
        document.body.appendChild(downloadLink);
        
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }

    function exportToPDF(myTableId)
    {
        var sTable = document.getElementById('tab-content').innerHTML;
        // var page = document.getElementById('page').getAttribute('data-page');

        var style = "<style>";
        style = style + "table {width: 100%;font: 17px Calibri;}";
        style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
        style = style + "padding: 2px 3px;text-align: center;}";
        style = style + "</style>";

        // CREATE A WINDOW OBJECT.
        var win = window.open('', '#', 'height=1000,width=2000');

        win.document.write('<html><head>');
        win.document.write('<title>Gigha Store</title>');   // <title> FOR PDF HEADER.
        win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
        win.document.write('</head>');
        win.document.write('<body>');
        win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
        // win.document.write('<small>page '+ page +'</small>');
        win.document.write('</body></html>');

        win.document.close();   // CLOSE THE CURRENT WINDOW.

        win.print(); // PRINT THE CONTENTS.
    }

</script>

</body>
</html>