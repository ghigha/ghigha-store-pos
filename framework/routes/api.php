<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// This crud api key
Route::post('apikey/create', 'ApiServer\ApiKeyController@create')->name('apikey.create');
Route::post('apikey/update/{id?}', 'ApiServer\ApiKeyController@update')->name('apikey.update');
Route::get('apikey/delete/{id}', 'ApiServer\ApiKeyController@delete')->name('apikey.delete');

// This produk
Route::get('produk', 'ApiServer\ProducController@produk')->name('produk');
Route::get('produk/detail/{id}', 'ApiServer\ProducController@detail')->name('produk.detail');
Route::post('produk/create', 'ApiServer\ProducController@create')->name('produk.create');
Route::post('produk/update/{id}', 'ApiServer\ProducController@update')->name('produk.update');
Route::get('produk/delete/{id}', 'ApiServer\ProducController@delete')->name('produk.delete');
// ............................................................................................................

// This supplier
Route::get('supplier', 'ApiServer\SupplierController@supplier')->name('supplier');
Route::get('supplier/detail/{id}', 'ApiServer\SupplierController@detail')->name('supplier.detail');
Route::post('supplier/create', 'ApiServer\SupplierController@create')->name('supplier.create');
Route::post('supplier/update/{id}', 'ApiServer\SupplierController@update')->name('supplier.update');
Route::get('supplier/delete/{id}', 'ApiServer\SupplierController@delete')->name('supplier.delete');
// ............................................................................................................

// This pembeli
Route::get('pembeli', 'ApiServer\PembeliController@pembeli')->name('pembeli');
Route::get('pembeli/detail/{id}', 'ApiServer\PembeliController@detail')->name('pembeli.detail');
Route::post('pembeli/create', 'ApiServer\PembeliController@create')->name('pembeli.create');
Route::post('pembeli/update/{id}', 'ApiServer\PembeliController@update')->name('pembeli.update');
Route::get('pembeli/delete/{id}', 'ApiServer\PembeliController@delete')->name('pembeli.delete');
// ............................................................................................................

// This varian
Route::get('varian', 'ApiServer\VarianController@varian')->name('varian');
Route::get('varian/detail/{id}', 'ApiServer\VarianController@detail')->name('varian.detail');
Route::post('varian/create', 'ApiServer\VarianController@create')->name('varian.create');
Route::post('varian/update/{id}', 'ApiServer\VarianController@update')->name('varian.update');
Route::get('varian/delete/{id}', 'ApiServer\VarianController@delete')->name('varian.delete');
// ............................................................................................................

// This operator
Route::get('operator', 'ApiServer\OperatorController@operator')->name('operator');
Route::post('operator/create', 'ApiServer\OperatorController@create')->name('operator.create');
Route::post('operator/update/{id}', 'ApiServer\OperatorController@update')->name('operator.update');
Route::get('operator/delete/{id}', 'ApiServer\OperatorController@delete')->name('operator.delete');
// ............................................................................................................

// This penjualan
Route::get('penjualan', 'ApiServer\PenjualanController@penjualan')->name('penjualan');
Route::get('penjualan/detail/{id}', 'ApiServer\PenjualanController@detail')->name('penjualan.detail');
Route::get('penjualan/delete/{id}', 'ApiServer\PenjualanController@delete')->name('penjualan.delete');
