<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'userController@dashboard');
    //Home
    Route::get('/home', 'userController@dashboard')->name('home');
    Route::post('/change', 'userController@changePassword')->name('changePassword');
    //Operator 
    Route::get('/operator', 'operatorController@index')->name('operator');
    Route::get('/operator/add', 'operatorController@add')->name('operator.add');
    Route::post('/operator', 'operatorController@store')->name('operator.store');
    Route::delete('/operator/{id}', 'operatorController@delete')->name('operator.delete');

    //Supplier 
    Route::get('/supplier', 'supplierController@index')->name('supplier');
    Route::post('/supplier', 'supplierController@store')->name('supplier.store');
    Route::put('/supplier/{id}', 'supplierController@update')->name('supplier.update');
    Route::delete('/supplier/{id}', 'supplierController@delete')->name('supplier.delete');
    Route::get('/supplier/detail/{id}', 'supplierController@detail')->name('supplier.detail');

    // variant
    Route::get('/variant', 'variantController@index')->name('variant');
    Route::post('/variant', 'variantController@store')->name('variant.store');
    Route::put('/variant/{id}', 'variantController@update')->name('variant.update');
    Route::delete('/variant/{id}', 'variantController@delete')->name('variant.delete');
    Route::get('/variant/detail/{id}', 'variantController@detail')->name('variant.detail');

    //Produk 
    Route::get('/produk', 'produkController@index')->name('produk');
    Route::get('/produk/add', 'produkController@add')->name('produk.add');
    Route::post('/produk', 'produkController@store')->name('produk.store');
    Route::put('/produk/{id}', 'produkController@update')->name('produk.update');
    Route::get('/produk/edit/{id}', 'produkController@edit')->name('produk.edit');
    Route::delete('/produk/{id}', 'produkController@delete')->name('produk.delete');
    Route::post('/produk/stok', 'produkController@add_stok')->name('produk.add_stok');
    Route::get('/produk/detail/{id}', 'produkController@detail')->name('produk.detail');
    Route::post('/produk/cancel', 'produkController@cancel_stok')->name('produk.cancel_stok');


    Route::get('/pembeli', 'pembeliController@index')->name('pembeli');
    Route::get('/pembeli/add', 'pembeliController@add')->name('pembeli.add');
    Route::post('/pembeli', 'pembeliController@store')->name('pembeli.store');
    Route::put('/pembeli/{id}', 'pembeliController@update')->name('pembeli.update');
    Route::get('/pembeli/edit/{id}', 'pembeliController@edit')->name('pembeli.edit');
    Route::delete('/pembeli/{id}', 'pembeliController@delete')->name('pembeli.delete');
    Route::get('/pembeli/detail/{id}', 'pembeliController@detail')->name('pembeli.detail');

    Route::get('/penjualan', 'penjualanController@index')->name('penjualan');
    Route::get('/cekongkir', 'userController@cekongkir')->name('cekongkir');
    Route::get('/laporan', 'userController@laporan')->name('laporan');
    Route::get('/penjualan/add', 'penjualanController@add')->name('penjualan.add');
    Route::post('/penjualan', 'penjualanController@store')->name('penjualan.store');
    Route::put('/penjualan/{id}', 'penjualanController@update')->name('penjualan.update');
    Route::get('/penjualan/edit/{id}', 'penjualanController@edit')->name('penjualan.edit');
    Route::delete('/penjualan/{id}', 'penjualanController@delete')->name('penjualan.delete');
    Route::get('/penjualan/detail/{id}', 'penjualanController@detail')->name('penjualan.detail');
    Route::put('/penjualan/status/{id}', 'penjualanController@update_stat')->name('penjualan.update_stat');
});
