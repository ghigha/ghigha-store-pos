<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan</title>
    <style>
        *{
            padding: 0px;
            margin: 0px;
            font-family: Arial, Helvetica, sans-serif;
        }
        body{
            background-color: rgba(0,0,0,0.1);
            padding: 20px 20px;
            text-align:center;
        }
        .wrapper{
            background-color: white;
            width: 100%;
            padding: 20px 0px;
            text-align: center;
            min-height:550px;
        }
        h3{
            margin-bottom: 20px;
        }
        td{
            padding: 5px ;
        }
        table{
            margin: 0px auto;
        }
        a{
            display:block;
            margin-bottom:20px;
            padding:10px;
            background-color: rgba(0,0,0,0.6);
            color:white;
            text-decoration:none;
        }
        @media  print {
         a {display:none}
      }
    </style>
</head>
<body>
    <a href="<?php echo e(route('laporan')); ?>" style="margin-bottom:5px">Kembali</a>
    <a onclick="window.print()" href="#">Preview</a>
    <div class="wrapper">
        <h3>Rekap penjualan | <?php echo e($arr[0]); ?> s/d <?php echo e($arr[1]); ?></h3>
        <table border="1" cellspacing="0">
            <tr>
                <td rowspan="2" style="width: 100px;font-weight:bold">No Pesanan</td>
                <td rowspan="2" style="width: 100px;font-weight:bold">Nama</td>
                <td rowspan="2" style="font-weight:bold">Metode</td>
                <td rowspan="2" style="font-weight:bold">Status</td>
                <td rowspan="2" style="width: 120px;font-weight:bold">No Resi</td>
                <td rowspan="2" style="font-weight:bold">Expedisi</td>
                <td rowspan="2" style="font-weight:bold">Ongkir</td>
                <td rowspan="2" style="font-weight:bold">Admin</td>
                <td style="width: 300px;font-weight:bold" colspan="3">Produk</td>
                <td rowspan="2" style="font-weight:bold">Jumlah Total</td>
            </tr>
            <tr>
                <td style="width:120px;font-weight:bold">Nama</td>
                <td style="font-weight:bold">Qty</td>
                <td style="font-weight:bold">Jumlah</td>
            </tr>
            <?php $semua = 0; ?>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $length = count($dt->listProduk) ?>
                <tr>
                    <td rowspan="<?php echo e($length); ?>"><?php echo e($dt->no_pesanan); ?></td>
                    <td rowspan="<?php echo e($length); ?>"><?php echo e($dt->nama_pembeli); ?></td>
                    <td rowspan="<?php echo e($length); ?>" ><?php echo e($dt->nama_method_pem); ?></td>
                    <td rowspan="<?php echo e($length); ?>" ><?php echo e($dt->nama_stat); ?></td>
                    <td rowspan="<?php echo e($length); ?>"><?php echo e($dt->no_resi); ?></td>
                    <td rowspan="<?php echo e($length); ?>" ><?php echo e($dt->expedisi); ?></td>
                    <td rowspan="<?php echo e($length); ?>" ><?php echo e($dt->ongkir); ?></td>
                    <td rowspan="<?php echo e($length); ?>" ><?php echo e($dt->admin); ?></td>
                    <td ><?php echo e($dt->listProduk[0]->nama_produk); ?></td>
                    <td ><?php echo e($dt->listProduk[0]->qty); ?></td>
                    <td ><?php echo e($dt->listProduk[0]->qty * $dt->listProduk[0]->harga); ?></td>
                    <td rowspan="<?php echo e($length); ?>" ><?php echo e($dt->jumlah_total); ?></td>
                </tr>
                <?php  $semua += $dt->jumlah_total;?>
                <?php if($length > 1): ?>
                <?php for($i=1;$i<$length;$i++): ?>
                <tr>
                    <td ><?php echo e($dt->listProduk[$i]->nama_produk); ?></td>
                    <td ><?php echo e($dt->listProduk[$i]->qty); ?></td>
                    <td ><?php echo e($dt->listProduk[$i]->qty * $dt->listProduk[$i]->harga); ?></td>
                </tr>
                <?php endfor; ?>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td colspan="11" style="font-weight:bold">Jumlah pendapatan  <?php echo e($arr[0]); ?> s/d <?php echo e($arr[1]); ?></td>
                <td colspan="11"><?php echo e($semua); ?></td>
            </tr>
        </table>
    </div>
</body>
</html><?php /**PATH D:\project\SReport\resources\views/preview.blade.php ENDPATH**/ ?>