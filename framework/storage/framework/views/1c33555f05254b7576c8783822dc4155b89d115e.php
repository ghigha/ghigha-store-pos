
<?php $__env->startSection('content'); ?>
 <div class="row justify-content-center">
   <div class="col-lg-10">
     <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ubah Pembeli</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?php echo e(route('pembeli.update', Crypt::encrypt($pembeli->id))); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('PUT')); ?>

                <div class="card-body">
                  <div class="row justify-content-end">
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Nama</label> <small><i class="text-danger"><?php echo e(($errors->has('nama_pembeli'))?$errors->first('nama_pembeli') : ""); ?></i></small>
                            <input type="text" name="nama_pembeli" class="form-control" value="<?php echo e($pembeli->nama_pembeli); ?>" required>
                        </div>
                        <div class="form-group">
                            <label >Nomor Hp</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('nomor_hp'))?$errors->first('nomor_hp') : ""); ?></i></small>
                            <div class="input-group">
                                <div class="input-group-prepend pr-2">
                                <span class="input-group-text bg-white">+62</span>
                                </div>
                                <input type="text" name="nomor_hp" class="form-control" value="<?php echo e($pembeli->nomor_hp); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Provinsi</label> <small><i class="text-danger"><?php echo e(($errors->has('provinsi'))?$errors->first('provinsi') : ""); ?></i></small>
                            <input type="text" name="provinsi" class="form-control" value="<?php echo e($pembeli->provinsi); ?>">
                        </div>
                        <div class="form-group">
                            <label >Kabupaten</label> <small><i class="text-danger"><?php echo e(($errors->has('kabupaten'))?$errors->first('kabupaten') : ""); ?></i></small>
                            <input type="text" name="kabupaten" class="form-control" value="<?php echo e($pembeli->kabupaten); ?>">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Alamat lengkap</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('alamat'))?$errors->first('alamat') : ""); ?></i></small>
                            <textarea name="alamat" class="form-control" cols="30" rows="8"><?php echo e($pembeli->alamat); ?></textarea>
                        </div>
                        <div class="form-group">
                            <label >Kode pos</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('kode_pos'))?$errors->first('kode_pos') : ""); ?></i></small>
                            <input type="number" name="kode_pos" class="form-control w-25" value="<?php echo e($pembeli->kode_pos); ?>">
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
   </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ghighac1/ghigha.my.id/framework/resources/views/operator/pembeli/edit.blade.php ENDPATH**/ ?>