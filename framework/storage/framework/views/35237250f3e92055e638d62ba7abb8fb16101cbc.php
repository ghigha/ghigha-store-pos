
<?php $__env->startSection('content'); ?>
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="<?php echo e(url()->previous()); ?>"></a>Detail <?php echo e($pembeli->nama_pembeli); ?></h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row justify-content-center">
            <div class="col-11">
              <div class="row mb-3">
                <div class="col-lg-6">
                    <small>Detail</small>
                    <a class="badge badge-danger float-right" href="" data-toggle="modal" data-target="#hapus-pembeli">
                        <i class="fas fa-trash mr-1"></i>Hapus
                    </a>
                    <a class="badge badge-warning float-right mr-1" href="<?php echo e(route('pembeli.edit', Crypt::encrypt($pembeli->id))); ?>">
                        <i class="fas fa-edit mr-1"></i>Ubah
                    </a>
                    <hr class="mt-0">
                   <label style="min-width:100px;">Nama </label><span>: <?php echo e($pembeli->nama_pembeli); ?></span>
                   <br>
                   <label style="min-width:100px;">Nomor Hp </label><span>: +62<?php echo e($pembeli->nomor_hp); ?></span>
                   <a class="badge badge-primary ml-1" target="_blank" href="https://wa.me/62<?php echo e($pembeli->nomor_hp); ?>">
                        <i class="fab fa-whatsapp"></i> chat
                    </a>
                   <br>
                   <label style="min-width:100px;">Provinsi </label><span>: <?php echo e($pembeli->provinsi); ?></span>
                   <br>
                   <label style="min-width:100px;">Kabupaten </label><span>: <?php echo e($pembeli->kabupaten); ?></span>
                   <br>
                   <small class="d-lg-inline-block d-none">Ditambahkan oleh <?php echo e($pembeli->name); ?></small>
                </div>
                <div class="mt-0 col-lg-6 mt-lg-4">
                    <hr class="mt-0 d-lg-block d-none">
                    <div>
                        <label class="float-left d-block" style="min-width:100px;height:80px;">Alamat </label>
                        <span class=" d-block" style="min-width:100px;min-height:80px;">: <?php echo e($pembeli->alamat); ?></span>
                    </div>
                   <br>
                   <label style="min-width:100px;">Kode pos </label><span>: <?php echo e($pembeli->kode_pos); ?></span>
                   <br>
                   <small class="d-lg-none d-inline-block">Ditambahkan oleh <?php echo e($pembeli->name); ?></small>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\project\SReport\resources\views/operator/pembeli/detail.blade.php ENDPATH**/ ?>