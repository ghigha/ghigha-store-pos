
<?php $__env->startSection('content'); ?>
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="<?php echo e(url()->previous()); ?>"></a>Edit penjualan</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
      <form action="<?php echo e(route('penjualan.update', Crypt::encrypt($penjualan->id))); ?>" method="POST">
            <?php echo e(csrf_field()); ?>

            <?php echo e(method_field('PUT')); ?>

          <div class="row justify-content-center">
            <div class="col-lg-4">
                <small>Pembeli*</small>
                <div class="input-group mb-2">
                    <select name="id_pembeli" class="form-control select2" required>
                        <option value="" disabled selected>-Pilih Pembeli-</option>
                        <?php $__currentLoopData = $pembeli; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($pem->id); ?>" <?php echo e($penjualan->id_pembeli === $pem->id ? "selected" : ""); ?>>
                            <?php echo e($pem->nama_pembeli); ?>

                        </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="row">
                    <div class="col-6">
                        <small>Metode pembayaran*</small>
                        <div class="input-group mb-2 mt-2">
                            <input type="radio" id="metode" name="id_method" value="1" <?php echo e($penjualan->id_method === 1 ? "checked" : ""); ?>>
                            <small for="">
                                Transfer
                            </small>
                            <input type="radio" name="id_method" value="2" class="ml-3" <?php echo e($penjualan->id_method === 2 ? "checked" : ""); ?>>
                            <small for="">
                                COD
                            </small>
                        </div>
                    </div>
                    <div class="col-6">
                        <small>Biaya Admin*</small>
                        <div class="input-group mb-2">
                            <input type="number" id="biaya-admin" name="admin" class="form-control" value="<?php echo e($penjualan->admin); ?>">
                            <input type="hidden" name="prev_admin" value="<?php echo e($penjualan->admin); ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <small>Expedisi pengiriman</small>
                        <div class="input-group mb-2">
                            <select name="expedisi" class="form-control">
                                <option value="" disabled selected>-Pilih Expedisi-</option>
                                <option value="JNE Express" <?php echo e($penjualan->expedisi === "JNE Express" ? "selected" : ""); ?>>
                                    JNE Express
                                </option>
                                <option value="J&T Express" <?php echo e($penjualan->expedisi === "J&T Express" ? "selected" : ""); ?>>
                                    J&T Express
                                </option>
                                <option value="Ninja Express" <?php echo e($penjualan->expedisi === "Ninja Express" ? "selected" : ""); ?>>
                                    Ninja Express
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <small>Ongkos Kirim*</small>
                        <div class="input-group mb-2">
                            <input type="text" name="ongkir" id="ongkir" class="form-control" value="<?php echo e($penjualan->ongkir); ?>" required>
                            <input type="hidden" name="prev_ongkir" value="<?php echo e($penjualan->ongkir); ?>">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-lg-4">
                <small>Nomor Resi</small>
                <div class="input-group mb-2">
                    <input type="text" name="no_resi" class="form-control" value="<?php echo e($penjualan->no_resi); ?>">
                </div>
                <small>Status*</small>
                <div class="input-group mb-2">
                    <select name="id_stat" class="form-control" required>
                        <option value="" selected disabled>-Pilih Status-</option>
                        <?php $__currentLoopData = $stats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($stat->id); ?>" <?php echo e($penjualan->id_stat === $stat->id ? "selected" : ""); ?>>
                            <?php echo e($stat->nama_stat); ?>

                        </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <button class="btn btn-primary form-control" type="submit">
                   <i class="fas fa-save"></i> Simpan
                </button>
            </div>
          </div>
          <!-- /.row -->
          </form>
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/betechde/ghigha.betechdeveloper.com/resources/views/operator/penjualan/edit.blade.php ENDPATH**/ ?>