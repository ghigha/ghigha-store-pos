<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo e(env('APP_NAME')); ?> | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/toastr/toastr.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/dist/css/adminlte.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo e(url('template')); ?>/plugins/summernote/summernote-bs4.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style type="text/css">
  .btn-lebar{
      display: none;
  }
  .btn-kecil{
      display: none;
  }
   @media(max-width: 850px) {
        .btn-lebar{
            display: none;
        }
        .btn-kecil{
            display: inline-block;
        }
        .profile-username{
          font-size: 14px;
          height: 40px;
          vertical-align: middle;
        }

    }

    .loader{
      width: 20px;
      height:20px;
      border-radius: 50%;
      display: none;
    }
    .loader-light{
      border:4px solid white;
      border-right:4px solid grey;
      animation: loader 1s infinite;
    }
    .show{
      display: inline-block;
    }

    .baru{
      animation: hig 1s linear;
      cursor: pointer;
    }

    @keyframes  hig{
      0%{
       background-color: rgba(0,0,0,0.3);
      }
      100%{
       background-color: 'white' !important;
      }
    }

    @keyframes  loader{
      0%{
        transform: rotate(0deg);
      }
      100%{
        transform: rotate(360deg);
      }
    }
    .wp-block{
      background-color: rgba(0,0,0,0.5);
      position: absolute;
      z-index: 99;
      top: 0px;
      bottom: 0px;
      left:0px;
      right: 0px;
      display: none;
    }
    .select2-container--default .select2-selection--single{
      height:38px;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i>
        <h4 class="brand-text font-weight-light btn-kecil ml-2"><?php echo e(env('APP_NAME')); ?></h4>
        </a>
      </li>
    </ul>
        <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
    <img src="<?php echo e(url('template')); ?>/dist/img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
      <span class="brand-text font-weight-light">
         <?php echo e(env('APP_NAME')); ?>

      </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
        <?php if(Auth::user()->role === 2): ?>
        <label class="text-light">Admin</label><br>
        <?php else: ?>
        <label class="text-light">Operator</label><br>
        <?php endif; ?>
          <a href="" class="badge badge-primary text-light"><i class="fas fa-user-edit mr-1"></i>Ubah Password</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php if(intval(Auth::user()->role) == 0): ?>
          <li class="nav-item">
            <a href="<?php echo e(route('home')); ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard 
              </p>
            </a>
          </li>
          <?php endif; ?>
          <li class="nav-item">
            <a href="<?php echo e(route('penjualan')); ?>" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Penjualan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo e(route('cekongkir')); ?>" class="nav-link">
              <i class="nav-icon fas fa-box"></i>
              <p>
                Cek Ongkir
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <?php if(intval(Auth::user()->role) == 0): ?>
              <li class="nav-item">
                <a href="<?php echo e(route('supplier')); ?>" class="nav-link">
                  <i class="fas fa-user-tie nav-icon"></i>
                  <p>Kelola Supplier</p>
                </a>
              </li>
              <?php endif; ?>
              <li class="nav-item">
                <a href="<?php echo e(route('pembeli')); ?>" class="nav-link">
                  <i class="fas fa-user nav-icon"></i>
                  <p>Kelola Pembeli</p>
                </a>
              </li>
              <?php if(intval(Auth::user()->role) == 0): ?>
              <li class="nav-item">
                <a href="<?php echo e(route('variant')); ?>" class="nav-link">
                  <i class="fas fa-box-open nav-icon"></i>
                  <p>Kelola variant</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo e(route('produk')); ?>" class="nav-link">
                  <i class="fas fa-box-open nav-icon"></i>
                  <p>Kelola Produk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo e(route('operator')); ?>" class="nav-link">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Kelola Operator</p>
                </a>
              </li>
              <?php endif; ?>
            </ul>
          </li>
          <?php if(intval(Auth::user()->role) == 0): ?>
          <li class="nav-item">
            <a href="<?php echo e(route('laporan')); ?>" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Laporan
              </p>
            </a>
          </li>
          <?php endif; ?>
          <li class="nav-item">
              <a class="nav-link logout" href="<?php echo e(route('logout')); ?>" >
                <i class="nav-icon fas fa-sign-out-alt"></i>
                 <p>Keluar</p>
              </a>

              <form id="formlogout" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                  <?php echo csrf_field(); ?>
              </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content pt-2">

      <?php echo $__env->yieldContent('content'); ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; <a href="http://betechdeveloper.com">2020</a> .</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo e(url('template')); ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(url('template')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(url('template')); ?>/plugins/chart.js/Chart.min.js"></script>
<script src="<?php echo e(url('template')); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo e(url('template')); ?>/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo e(url('template')); ?>/dist/js/adminlte.min.js"></script>
<script src="<?php echo e(url('template')); ?>/plugins/select2/js/select2.full.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(url('template')); ?>/dist/js/demo.js"></script>
<!-- InputMask -->
<script src="<?php echo e(url('template')); ?>/plugins/moment/moment.min.js"></script>
<!-- FLOT CHARTS -->
<!-- date-range-picker -->
<script src="<?php echo e(url('template')); ?>/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo e(url('template')); ?>/plugins/flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo e(url('template')); ?>/plugins/flot-old/jquery.flot.resize.min.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?php echo e(url('template')); ?>/plugins/flot-old/jquery.flot.pie.min.js"></script>
<script src="<?php echo e(url('template')); ?>/plugins/summernote/summernote-bs4.min.js"></script>
<script>
$(function () {
    $('.select2').select2();
  });
  $('.logout').click(function(){
    event.preventDefault();
     Swal.fire({
      title : 'Keluar Dari <?php echo e(env("APP_NAME")); ?> ?',
      showCancelButton : true,
      confirmButtonText : 'Keluar',
      cancelButtonText : 'Batal'
    }).then((result)=> {
      if(result.value){
        $('#formlogout').submit();
      }
    });
  })
//Date range picker
$('#reservation').daterangepicker({
  locale: {
            format: 'YYYY-MM-DD'
        }
})
  //Date range as a button
  $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

  $(function () {
    // Summernote
    $('.textarea').summernote({
      toolbar : [
        ['para', ['ol','ul']],
        ['view', ['fullscreen','codeview']]
      ]
    })
  })
</script>
<?php if(session('sukses')): ?>
<script type="text/javascript">
  $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 4000
    });

  
      Toast.fire({
        type: 'success',
        title: '<?php echo e(session("sukses")); ?>'
      })
  });
</script>
<?php endif; ?>
<?php if(session('error')): ?>
<script type="text/javascript">
  $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 4000
    });

  
      Toast.fire({
        type: 'error',
        title: '<?php echo e(session("error")); ?>'
      })
  });
</script>
<?php endif; ?>
<?php echo $__env->yieldPushContent('js'); ?>
</body>
</html>
<?php /**PATH /home/betechde/ghigha.betechdeveloper.com/resources/views/app/app.blade.php ENDPATH**/ ?>