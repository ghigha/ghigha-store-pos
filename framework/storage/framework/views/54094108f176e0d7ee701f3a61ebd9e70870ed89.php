
<?php $__env->startSection('content'); ?>
 <div class="row justify-content-center">
   <div class="col-lg-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="nav-icon fas fa-file mr-2"></i> Laporan</h3>
            </div>
            <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <small>Masukan jenjang waktu</small>
                        <form action="<?php echo e(route('laporan')); ?>">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <input type="text" name="range" class="form-control float-right text-center" id="reservation">
                                </div>
                                <div class="col-6 mt-2">
                                    <button type="submit" class="btn btn-primary form-control">Lihat</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
   </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ghighac1/ghigha.my.id/framework/resources/views/laporan.blade.php ENDPATH**/ ?>