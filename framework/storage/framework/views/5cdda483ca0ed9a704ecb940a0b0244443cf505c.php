
<?php $__env->startSection('content'); ?>
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="<?php echo e(url()->previous()); ?>"></a>Detail <?php echo e($produk->nama_produk); ?></h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row justify-content-center">
            <div class="col-11">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <small>Detail</small>
                    <a class="badge badge-danger float-right" href="" data-toggle="modal" data-target="#hapus-produk">
                        <i class="fas fa-trash mr-1"></i>Hapus
                    </a>
                    <a class="badge badge-warning float-right mr-1" href="" data-toggle="modal" data-target="#ubah-produk">
                        <i class="fas fa-edit mr-1"></i>Ubah
                    </a>
                    <hr class="mt-0">
                   <label style="min-width:100px;">Nama Produk </label><span>: <?php echo e($produk->nama_produk); ?></span>
                   <br>
                   <label style="min-width:100px;">Harga </label><span>: <?php echo e($produk->harga); ?></span>
                   <br>
                   <label style="min-width:100px;">Harga Jual</label><span>: <?php echo e($produk->harga_beli); ?></span>
                   <br>
                   <label style="min-width:100px;">variant</label><span>: <?php echo e($produk->nama_variant); ?></span>
                   <br>
                   <label style="min-width:100px;">warna</label><span>: <?php echo e($produk->warna); ?></span>
                   <br>
                   <label style="min-width:100px;">size</label><span>: <?php echo e($produk->size); ?></span>
                   <br>
                   <label style="min-width:100px;">Stok </label><span>: <?php echo e($produk->stok); ?></span>
                   <a class="badge badge-primary ml-1" href="" data-toggle="modal" data-target="#tambah-stok">
                        <i class="fas fa-plus"></i>
                    </a>
                   <br>
                   <label style="min-width:100px;">Supplier </label><span>: <?php echo e($produk->nama_supplier); ?></span>
                   <br>
                   <small>Ditambahkan pada <?php echo e(Carbon\Carbon::parse($produk->created_at)->format('d-m-Y')); ?></small>
                </div>
                <div class="col-lg-8 mt-lg-0 mt-4">
                    <small>Riwayat stok</small>
                    <hr class="mt-0">
                    <div class="timeline">
                        <?php $__currentLoopData = $riwayat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $riw): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div>
                            <i class="fas fa-box bg-info"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fas fa-clock"></i> <?php echo e(Carbon\Carbon::parse($riw->created_at)->format('d-m-Y')); ?></span>
                                <h3 class="timeline-header no-border">
                                    Stok <a class="text-info"><?php echo e($produk->nama_produk); ?></a>   
                                    ditambah sebanyak <?php echo e($riw->stok); ?>

                                    <br> 
                                    Total stok <?php echo e($riw->hasil); ?>

                                    <br>
                                    <?php if($key === 0): ?>
                                    <form action="<?php echo e(route('produk.cancel_stok')); ?>" method="POST">
                                        <?php echo csrf_field(); ?>
                                        <input type="hidden" name="id_stok" value="<?php echo e($riw->id); ?>">
                                        <input type="hidden" name="id_produk" value="<?php echo e($produk->id); ?>">
                                        <input type="hidden" name="prevStok" value="<?php echo e($riw->stok); ?>">
                                        <input type="hidden" name="stok" value="<?php echo e($produk->stok); ?>">
                                        <button type="submit" class="badge badge-info mt-1" style="border: none">
                                            <i class="fas fa-sync-alt mr-1"></i>Batalkan
                                        </button>
                                    </form>
                                    <?php endif; ?>
                                </h3>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div>
                            <i class="fas fa-clock bg-gray"></i>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
  <div class="modal fade" id="hapus-produk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Yakin ingin menghapus ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo e(route('produk.delete', Crypt::encrypt($produk->id))); ?>" method="POST">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('DELETE')); ?>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Yakin</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
  <div class="modal fade" id="ubah-produk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo e(route('produk.update', Crypt::encrypt($produk->id))); ?>" method="POST">
                <div class="modal-body">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('PUT')); ?>

                    <small>Nama</small>
                    <input type="text" class="form-control" name="nama_produk" value="<?php echo e($produk->nama_produk); ?>" required>
                    <small>Harga</small>
                    <input type="number" class="form-control" name="harga" value="<?php echo e($produk->harga); ?>" required>
                    <small>Harga jual</small>
                    <input type="number" class="form-control" name="harga_beli" value="<?php echo e($produk->harga_beli); ?>" required>
                    <small>Supplier</small>
                    <select name="id_supplier" class="form-control" required>
                        <option value="" selected disabled>-Pilih Supplier-</option>
                        <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($sup->id); ?>" <?php echo e($sup->id === $produk->id_supplier? "selected" : ""); ?>><?php echo e($sup->nama_supplier); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <small>Supplier</small>
                    <select name="id_variant" class="form-control" required>
                        <option value="" selected disabled>-Pilih variant-</option>
                        <?php $__currentLoopData = $variant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($sup->id); ?>" <?php echo e($sup->id === $produk->id_variant? "selected" : ""); ?>><?php echo e($sup->nama_variant); ?> - <?php echo e($sup->warna); ?> - <?php echo e($sup->size); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <?php echo e(csrf_field()); ?>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

  <!-- /.card -->
  <div class="modal fade" id="tambah-stok">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Stok</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo e(route('produk.add_stok')); ?>" method="POST">
                <div class="modal-body">
                    <?php echo e(csrf_field()); ?>

                    <small>Jumlah Stok</small>
                    <input type="text" class="form-control" name="stok" required>
                    <input type="hidden" class="form-control" name="id_produk" value="<?php echo e($produk->id); ?>">
                    <input type="hidden" class="form-control" name="prevStok" value="<?php echo e($produk->stok); ?>">
                </div>
                <?php echo e(csrf_field()); ?>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/betechde/ghigha.betechdeveloper.com/resources/views/admin/produk/detail.blade.php ENDPATH**/ ?>