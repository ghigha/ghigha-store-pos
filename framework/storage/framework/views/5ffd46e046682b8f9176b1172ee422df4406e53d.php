
<?php $__env->startSection('content'); ?>
 <div class="row justify-content-center">
   <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="nav-icon fas fa-box mr-2"></i> Cek Ongkir</h3>
            </div>
            <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <small>Cek ongkir J&T Express</small>
                                <form action="<?php echo e(route('cekongkir')); ?>">
                                    <div class="row">
                                        <div class="col-5">
                                            <select name="asal" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Asal-</option>
                                                <option value="Jakarta">Jakarta</option>
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <select name="id" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Tujuan-</option>
                                                <?php $__currentLoopData = $jnt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($pem->id); ?>"><?php echo e($pem->tujuan); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="col-2">
                                        <button type="submit" class="btn btn-primary">Cek</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <small>Cek ongkir Ninja Express</small>
                                <form action="<?php echo e(route('cekongkir')); ?>">
                                    <div class="row">
                                        <div class="col-5">
                                            <select name="asal" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Asal-</option>
                                                <option value="Jakarta">Jakarta</option>
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <select name="id" class="form-control select2" required>
                                                <option value="" disabled selected>-Pilih Tujuan-</option>
                                                <?php $__currentLoopData = $ninja; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($pem->id); ?>"><?php echo e($pem->tujuan); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="col-2">
                                        <button type="submit" class="btn btn-primary">Cek</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-4 mt-5">
                                <small>Ongkir</small>
                                <input type="text" class="form-control" value="<?php echo e($ongkir); ?>">
                            </div>
                            <div class="col-2 mt-5">
                            <form action="<?php echo e(route('penjualan.add')); ?>">
                                <input type="hidden" name="ongkir" class="form-control" value="<?php echo e($ongkir); ?>">
                                <input type="hidden" name="expedisi" class="form-control" value="<?php echo e($expedisi); ?>">
                            <br>
                                <button type="submit" class="btn btn-primary">Tambahkan</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/betechde/ghigha.betechdeveloper.com/resources/views/cek_ongkir.blade.php ENDPATH**/ ?>