
<?php $__env->startSection('content'); ?>
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><i class="fas fa-user-tie mr-1"></i>Kelola Supplier</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row">
            <div class="col-12">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <form action="<?php echo e(route('supplier')); ?>">
                      <div class="input-group">
                    <div class="custom-file">
                      <input type="text" name="key" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>
                </div>
                <div class="col-lg-8">
                  <a data-toggle="modal" data-target="#tambah-supplier" class="btn btn-primary float-lg-right mr-3 mt-3 mt-lg-0 text-light">
                  <i class="fas fa-plus mr-1"></i>Tambah Supplier</a>
                </div>
              </div>
              <small><i class="fas fa-list"></i> Daftar supplier</small>
              <hr class="mt-0">
              <div class="row">
                <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-2 col-6">
                    <div class="card text-center" style="height:90%">
                        <div class="card-body">
                            <a href="" class="d-block mb-2" style="line-height:19px;">
                                <label class="d-block" style="min-height:40px;"><?php echo e(Str::limit($sup->nama_supplier, 23)); ?></label>
                                <small class="d-block">dibuat <?php echo e(Carbon\Carbon::parse($sup->created_at)->format('d-m-Y')); ?></small>
                            </a>
                            <a class="badge badge-warning" href="" data-toggle="modal" data-target="#ubah-supplier<?php echo e($sup->id); ?>"><i class="fas fa-edit mr-1"></i>Ubah</a>
                            <a class="badge badge-danger" data-toggle="modal" data-target="#delete-supplier<?php echo e($sup->id); ?>" href=""><i class="fas fa-trash mr-1"></i>Hapus</a>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="delete-supplier<?php echo e($sup->id); ?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Yakin ingin menghapus ?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                              <label for="">Peringatan</label><br>
                              <span>Menghapus supplier akan otomatis menghapus data produk yang terhubung dengan supplier.</span>
                            </div>
                            <form action="<?php echo e(route('supplier.delete', Crypt::encrypt($sup->id))); ?>" method="POST">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('DELETE')); ?>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Yakin</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="ubah-supplier<?php echo e($sup->id); ?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Ubah Supplier</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="<?php echo e(route('supplier.update', Crypt::encrypt($sup->id))); ?>" method="POST">
                                <div class="modal-body">
                                    <?php echo e(csrf_field()); ?>

                                    <?php echo e(method_field('PUT')); ?>

                                    <small>Nama supplier</small>
                                    <input type="text" class="form-control" name="nama_supplier" value="<?php echo e($sup->nama_supplier); ?>">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Ubah</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->

<div class="modal fade" id="tambah-supplier">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Supplier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo e(route('supplier.store')); ?>" method="POST">
                <div class="modal-body">
                    <?php echo e(csrf_field()); ?>

                    <small>Nama supplier</small>
                    <input type="text" class="form-control" name="nama_supplier" value="<?php echo e(old('nama_supplier')); ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ghighac1/ghigha.my.id/framework/resources/views/admin/supplier/index.blade.php ENDPATH**/ ?>