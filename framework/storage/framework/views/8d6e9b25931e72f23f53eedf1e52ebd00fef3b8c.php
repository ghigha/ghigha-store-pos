
<?php $__env->startSection('content'); ?>
 <div class="row justify-content-center">
   <div class="col-lg-10">
     <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="<?php echo e(url()->previous()); ?>"></a>Tambah Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?php echo e(route('produk.store')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <div class="card-body">
                  <div class="row justify-content-end">
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Supplier</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('id_supplier'))?$errors->first('id_supplier') : ""); ?></i></small>
                            <select name="id_supplier" class="form-control">
                                <option value="" selected disabled>-Pilih Supplier-</option>
                                <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($sup->id); ?>"><?php echo e($sup->nama_supplier); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >variant</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('id_variant'))?$errors->first('id_variant') : ""); ?></i></small>
                            <select name="id_variant" class="form-control">
                                <option value="" selected disabled>-Pilih variant-</option>
                                <?php $__currentLoopData = $variant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($sup->id); ?>"><?php echo e($sup->nama_variant); ?> - <?php echo e($sup->warna); ?> - <?php echo e($sup->size); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Nama Produk</label> <small><i class="text-danger"><?php echo e(($errors->has('nama_produk'))?$errors->first('nama_produk') : ""); ?></i></small>
                            <input type="text" name="nama_produk" class="form-control" value="<?php echo e(old('nama_produk')); ?>">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label >Harga</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('harga'))?$errors->first('harga') : ""); ?></i></small>
                            <input type="number" name="harga" class="form-control" value="<?php echo e(old('harga')); ?>">
                        </div>
                        <div class="form-group">
                            <label >Harga jual</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('harga_beli'))?$errors->first('harga_beli') : ""); ?></i></small>
                            <input type="number" name="harga_beli" class="form-control" value="<?php echo e(old('harga_beli')); ?>">
                        </div>
                        <div class="form-group">
                            <label >Stok awal</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('stok'))?$errors->first('stok') : ""); ?></i></small>
                            <input type="number" name="stok" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
   </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/betechde/ghigha.betechdeveloper.com/resources/views/admin/produk/add.blade.php ENDPATH**/ ?>