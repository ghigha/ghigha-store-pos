
<?php $__env->startSection('content'); ?>
 <div class="row justify-content-center">
   <div class="col-lg-10">
     <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><a class="fas fa-arrow-left mr-4 text-dark" href="<?php echo e(url()->previous()); ?>"></a>Tambah Operator</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?php echo e(route('operator.store')); ?>" method="post">
                <?php echo e(csrf_field()); ?>

                <div class="card-body">
                  <div class="row justify-content-end">
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Nama</label> <small><i class="text-danger"><?php echo e(($errors->has('name'))?$errors->first('name') : ""); ?></i></small>
                            <input type="text" name="name" class="form-control" value="<?php echo e(old('name')); ?>">
                        </div>
                        <div class="form-group">
                            <label >Email</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('email'))?$errors->first('email') : ""); ?></i></small>
                            <input type="email" name="email" class="form-control" value="<?php echo e(old('email')); ?>">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label >Password</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('password'))?$errors->first('password') : ""); ?></i></small>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label >Konfirmasi Password</label> 
                            <small><i class="text-danger"><?php echo e(($errors->has('password_confirmation'))?$errors->first('password_confirmation') : ""); ?></i></small>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
   </div>
 </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\project\SReport\resources\views/admin/operator/add.blade.php ENDPATH**/ ?>