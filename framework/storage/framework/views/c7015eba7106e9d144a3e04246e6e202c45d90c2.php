
<?php $__env->startSection('content'); ?>
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><i class="fas fa-chart-bar mr-1"></i>Tambah penjualan</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
      <form action="<?php echo e(route('penjualan.store')); ?>" method="POST">
            <?php echo e(csrf_field()); ?>

          <div class="row">
            <div class="col-lg-4">
                <small>Pembeli*</small>
                <div class="input-group mb-2">
                    <select name="id_pembeli" class="form-control select2" required>
                        <option value="" disabled selected>-Pilih Pembeli-</option>
                        <?php $__currentLoopData = $pembeli; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($pem->id); ?>"><?php echo e($pem->nama_pembeli); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="row">
                    <div class="col-6">
                        <small>Metode pembayaran*</small>
                        <div class="input-group mb-2 mt-2">
                            <input type="radio" id="metode" name="id_method" value="1" required><small for="">Transfer</small>
                            <input type="radio" name="id_method" value="2" class="ml-3"><small for="">COD</small>
                        </div>
                    </div>
                    <div class="col-6">
                        <small>Biaya Admin*</small>
                        <div class="input-group mb-2">
                            <input type="number" id="biaya-admin" name="admin" class="form-control" value="0">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <small>Expedisi pengiriman</small>
                        <div class="input-group mb-2">
                            <select name="expedisi" class="form-control">
                                <option value="" disabled selected>-Pilih Expedisi-</option>
                                <option value="JNE Express" <?php echo e($expedisi === "JNE Express" ? "selected" : ""); ?>>JNE Express</option>
                                <option value="J&T Express" <?php echo e($expedisi === "J&T Express" ? "selected" : ""); ?>>J&T Express</option>
                                <option value="Ninja Express" <?php echo e($expedisi === "Ninja Express" ? "selected" : ""); ?>>Ninja Express</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <small>Ongkos Kirim*</small>
                        <div class="input-group mb-2">
                            <input type="text" name="ongkir" id="ongkir" class="form-control" value="<?php echo e($ongkir); ?>" required>
                        </div>
                    </div>
                </div>
                <small>Nomor Resi</small>
                <div class="input-group mb-2">
                    <input type="text" name="no_resi" class="form-control">
                </div>
                <small>Status*</small>
                <div class="input-group mb-2">
                    <select name="id_stat" class="form-control" required>
                        <option value="" selected disabled>-Pilih Status-</option>
                        <?php $__currentLoopData = $stats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($stat->id); ?>"><?php echo e($stat->nama_stat); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-lg-4">
                <!-- <small>Cari Produk</small>
                <div class="input-group mb-2">
                    <input type="text" class="form-control" placeholder="Cari berdasarkan nama produk..">
                </div> -->
                <small>Pilih Produk*</small><small><i class="text-danger"><?php echo e(($errors->has('id_produk'))?$errors->first('id_produk') : ""); ?></i></small>
                <hr class="mt-0 mb-0">
                <div class="bg-light p-2" style="height:350px;overflow-x: hidden; 
                overflow-y: auto; 
                text-align:justify;">
                    <div class="row">
                        <?php $__currentLoopData = $produk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-6" id="produk<?php echo e($pro->id); ?>">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <span style="display:block;min-height:10px"><?php echo e($pro->nama_produk); ?></span>
                                    <span style="display:block;min-height:10px">warna : <?php echo e($pro->nama_variant); ?></span>
                                    <span style="display:block;min-height:10px">warna : <?php echo e($pro->warna); ?></span>
                                    <span style="display:block;min-height:10px">size : <?php echo e($pro->size); ?></span>
                                    <span style="display:block;min-height:10px">stok : <?php echo e($pro->stok); ?></span>
                                    <div class="input-group mt-2">
                                        <input type="number" placeholder="qty" class="form-control text-center qty" value="">
                                        <div class="input-group-append">
                                            <a class="btn btn-primary text-light add-product" href="" data-id="<?php echo e($pro->id); ?>" data-qty="<?php echo e($pro->stok); ?>"  data-harga="<?php echo e($pro->harga); ?>" data-beli="<?php echo e($pro->harga_beli); ?>" data-nama="<?php echo e($pro->nama_produk); ?>" data-variant="<?php echo e($pro->nama_variant); ?>" data-size="<?php echo e($pro->size); ?>" data-warna="<?php echo e($pro->warna); ?>" ><i class="fas fa-check"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
            <div id="hidden">
                <input name="jumlah_total" type="hidden" id="jumlah-total-input" value="0">
                <input name="jumlah_pokok" type="hidden" id="jumlah-total-pokok" value="0">
            </div>
            <div class="col-lg-4">
                <small>Preview</small>
                <hr class="mt-0">
                <div class="text-right">
                    <small>*klik produk untuk menghapus dari preview</small>
                </div>                
                <div class="table-responsive" style="max-height:350px">
                    <table class="table table-hover" id="table-preview">
                        <tr class="bg-dark text-white">
                            <td>Nama Barang</td>
                            <td style="width:20px">Qty</td>
                            <td>variant</td>
                            <td>size</td>
                            <td>warna</td>
                            <td>Harga</td>
                            <td>Harga Jual</td>
                            <td>Jumlah</td>
                        </tr>
                        <tr class="bg-light">
                            <td colspan="7" class="text-right">Ongkos kirim</td>
                            <td id="jumlah-ongkir"><?php echo e($ongkir === null ? 0 : $ongkir); ?></td>
                        </tr>
                        <tr class="bg-light">
                            <td colspan="7" class="text-right">Biaya admin</td>
                            <td id="tampil-admin">0</td>
                        </tr>
                        <tr style="border-top:2px solid black" class="bg-dark text-light">
                            <td colspan="7" class="text-right">Jumlah Total</td>
                            <td id="total-jumlah"><?php echo e($ongkir === null ? 0 : $ongkir); ?></td>
                        </tr>
                    </table>
                </div>
                <button class="btn btn-primary form-control" type="submit">
                   <i class="fas fa-save"></i> Simpan
                </button>
            </div>
          </div>
          <!-- /.row -->
          </form>
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
<?php $__env->startPush('js'); ?>
<!-- Select2 -->
    <script>
        function toRupiah(angka){
            var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return rupiah;
        }

        function toNormal(angka){
            return angka.replace(/[^\w\s]/gi, '');
        }
        $('.add-product').on('click', function(e){
            e.preventDefault();
            var qty = $(this).parent().siblings('.qty').val() || 1;
            var nama = $(this).attr('data-nama');
            var harga = $(this).attr('data-harga');
            var variant = $(this).attr('data-variant');
            var size = $(this).attr('data-size');
            var warna = $(this).attr('data-warna');
            var beli = $(this).attr('data-beli');
            var prev_qty = $(this).attr('data-qty');
            if(parseInt(qty) > parseInt(prev_qty)){
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 4000
                });
        
                Toast.fire({
                    type: 'error',
                    title: 'Maaf, stok tidak cukup, tolong kurangi qty '
                })
            }else{
                var id = $(this).attr('data-id');
                var jumlah = parseInt(beli) * parseInt(qty);
                var pokok = parseInt(harga) * parseInt(qty);
                var totalJumlah = parseInt(toNormal($('#total-jumlah').text()));
                var tr = '<tr data-id="'+id+'" class="baru"><td>'+nama+'</td><td>'+qty+'</td>'+'<td>'+variant+'</td>'+'<td>'+size+'</td>'+'<td>'+warna+'</td><td>'+toRupiah(harga)+'</td><td>'+toRupiah(beli)+'</td><td class="jumlah">'+toRupiah(jumlah)+'</td></tr>';
                $('#table-preview').find('tr').last().prev().prev().prev().after(tr);
                $('#total-jumlah').text(toRupiah(totalJumlah + jumlah));
                $('#jumlah-total-input').val(totalJumlah + jumlah);
                $('#jumlah-total-pokok').val(totalJumlah + pokok);
                $('#hidden').append('<input name="id_produk[]" value="'+id+'" type="hidden" class="hidden'+id+'">');
                $('#hidden').append('<input name="qty[]" value="'+qty+'" type="hidden" class="hidden'+id+'">');
                $('#produk'+id).hide(500);
            }
        });
        $('#biaya-admin').on('keyup', function(e){
            if(e.keyCode === 8){
                $(this).val('');
                var admin = parseInt(toNormal($('#tampil-admin').text())) || 0;
                var totalJumlah = parseInt(toNormal($('#total-jumlah').text())) || 0;
                $('#total-jumlah').text(toRupiah(totalJumlah - admin));
                $('#jumlah-total-input').val(totalJumlah - admin);
                $('#tampil-admin').text(0);

            }else{
                var admin = $(this).val() || 0;
                var tampilAdmin = parseInt(toNormal($('#tampil-admin').text())) || 0;
                var totalJumlah = parseInt(toNormal($('#total-jumlah').text())) || 0;
                var hasil = totalJumlah - tampilAdmin;
                $('#total-jumlah').text(toRupiah(hasil + parseInt(admin)));
                $('#jumlah-total-input').val(hasil + parseInt(admin));
                $('#jumlah-total-pokok').val(hasil + parseInt(admin));
                $('#tampil-admin').text(toRupiah(parseInt(admin)));
            }
        });

        $('#ongkir').on('keyup', function(e){
            if(e.keyCode === 8){
                $(this).val('');
                var ongkir = parseInt(toNormal($('#jumlah-ongkir').text())) || 0;
                var totalJumlah = parseInt(toNormal($('#total-jumlah').text())) || 0;
                $('#total-jumlah').text(toRupiah(totalJumlah - ongkir));
                $('#jumlah-total-input').val(totalJumlah - ongkir);
                $('#jumlah-ongkir').text(0);
            }else{
                var ongkir = $(this).val() || 0;
                var jumlahOngkir = parseInt(toNormal($('#jumlah-ongkir').text())) || 0;
                var totalJumlah = parseInt(toNormal($('#total-jumlah').text())) || 0;
                var hasil = totalJumlah - jumlahOngkir;
                $('#total-jumlah').text(toRupiah(hasil + parseInt(ongkir)));
                $('#jumlah-total-input').val(hasil + parseInt(ongkir));
                $('#jumlah-total-pokok').val(hasil + parseInt(ongkir));
                $('#jumlah-ongkir').text(toRupiah(parseInt(ongkir)));
            }
        });

        $('#table-preview').on('click', '.baru', function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            var jumlah = parseInt(toNormal($(this).children('.jumlah').text()));
            var totalJumlah = parseInt(toNormal($('#total-jumlah').text()));
            $('#produk'+id).show(500);
            $('#total-jumlah').text(toRupiah(totalJumlah - jumlah));
            $('#jumlah-total-input').val(totalJumlah - jumlah);
            $('#hidden').children('.hidden'+id).remove();
            $(this).remove();
        });
    </script>
<?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/betechde/ghigha.betechdeveloper.com/resources/views/operator/penjualan/add.blade.php ENDPATH**/ ?>