
<?php $__env->startSection('content'); ?>
 <div class="row justify-content-center">
   <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">  <i class="nav-icon fas fa-tachometer-alt mr-2"></i> Dashboard</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Produk</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;"><?php echo e($data['jml_produk']); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Supplier</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;"><?php echo e($data['jml_supplier']); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Pembeli</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;"><?php echo e($data['jml_pembeli']); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="card ">
                                <div class="card-body text-center p-2">
                                    <label>Jumlah Operator</label>
                                    <span style="font-size:40px;display:block;margin-top:-15px;"><?php echo e($data['jml_operator']); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <small>Jumlah penjualan 7 hari kebelakang</small>
                    <?php $d = '[' ?>
                    <?php $t = "[" ?>
                    <?php $max = 0 ?>
                    <?php for($x = 6; $x >= 0; $x--): ?>
                        <?php  $d .= $jml[$x]['jml'].','?>
                        <?php if($x===0): ?>
                        <?php $tmp = "Hari ini"; ?>
                        <?php else: ?>
                        <?php $tmp = \Carbon\Carbon::parse($jml[$x]['tgl'])->format('d-M'); ?>
                        <?php endif; ?>
                        <?php  $t .="'". $tmp ."',"?>
                        <?php if($max <= $jml[$x]['jml'] + 9): ?>
                            <?php $max = $jml[$x]['jml'] + 9 ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <?php $d .= ']' ?>
                    <?php $t .= "]" ?>
                      <div class="chart">
                        <canvas id="barChart" style="height:350px; min-height:230px"></canvas>
                    </div>
                    </div>
                    <div class="col-lg-4">
                    <small>Produk hampir habis</small>
                        <div class="bg-light p-2" style="height:450px;overflow-x: hidden; 
                    overflow-y: auto; 
                    text-align:justify;">
                            <div class="row">
                                <?php $__currentLoopData = $produk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-6">
                                    <div class="card ">
                                        <div class="card-body text-center p-2">
                                            <a style="display:block;min-height:50px" class="text-dark" href="<?php echo e(route('produk.detail', Crypt::encrypt($pro->id))); ?>">
                                                <?php echo e($pro->nama_produk); ?>

                                            </a>
                                            <label><?php echo e($pro->stok); ?> tersedia</label>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
 </div>
 <?php $__env->startPush('js'); ?>
 <script>
 var ctx = document.getElementById("barChart");
var myBarChart = new Chart(ctx, {
type: 'bar',
data: {
    labels  : <?= $t ?>,
    datasets: [
    {
        label               : 'Penjualan',
        backgroundColor     : 'rgba(60,141,188,0.9)',
        borderColor         : 'rgba(60,141,188,0.8)',
        pointRadius          : false,
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : <?php echo e($d); ?>,
    }
    ],
},
options: {
    maintainAspectRatio: false,
    layout: {
    padding: {
        bottom: 0
    }
    },
    scales: {
    xAxes: [{
        time: {
        unit: 'month'
        },
        gridLines: {
        display: false,
        drawBorder: false
        },
        ticks: {
        maxTicksLimit: 6
        },
        maxBarThickness: 200,
    }],
    yAxes: [{
        ticks: {
        min: 0,
        max: <?php echo e($max); ?>,
        padding: 10,
        // Include a dollar sign in the ticks
        callback: function(value, index, values) {
            return value;
        }
        },
        gridLines: {
        color: "rgb(234, 236, 244)",
        zeroLineColor: "rgb(234, 236, 244)",
        drawBorder: false,
        borderDash: [2],
        zeroLineBorderDash: [2]
        }
    }],
    },
    legend: {
    display: false
    },
    tooltips: {
    titleMarginBottom: 10,
    titleFontColor: '#6e707e',
    titleFontSize: 14,
    backgroundColor: "rgb(255,255,255)",
    bodyFontColor: "#858796",
    borderColor: '#dddfeb',
    borderWidth: 1,
    xPadding: 15,
    yPadding: 15,
    displayColors: false,
    caretPadding: 10,
    callbacks: {
        label: function(tooltipItem, chart) {
        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
        return datasetLabel + ': ' + tooltipItem.yLabel + ' kali';
        }
    }
    },
}
});

 </script>
 <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\project\SReport\resources\views/dashboard.blade.php ENDPATH**/ ?>