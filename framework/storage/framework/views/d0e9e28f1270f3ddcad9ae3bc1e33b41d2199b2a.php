
<?php $__env->startSection('content'); ?>
  <!-- Default box -->
  <div class="card">
    <div class="card-header " style="border-bottom: none">
      <h3 class="card-title"><i class="fas fa-box-open mr-1"></i>Kelola Produk</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">
      <!-- Table row -->
          <div class="row">
            <div class="col-12">
              <div class="row mb-3">
                <div class="col-lg-4">
                    <form action="<?php echo e(route('produk')); ?>">
                    <div class="input-group">
                    <div class="custom-file">
                        <input type="text" name="key" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>
                </div>
                <div class="col-lg-8">
                  <a href="<?php echo e(route('produk.add')); ?>" class="btn btn-primary float-lg-right mr-3 mt-3 mt-lg-0 text-light">
                  <i class="fas fa-plus mr-1"></i>Tambah Produk</a>
                </div>
              </div>
              <small><i class="fas fa-list"></i> Daftar Produk</small>
                <select name="" id="" class="form-control d-inline-block ml-3" style="width:100px;">
                    <option value="">A-Z</option>
                    <option value="">Z-A</option>
                    <option value="">Terbaru</option>
                    <option value="">Stok Hampir Habis</option>
                </select>
                <button type="submit" class="btn btn-primary d-inline-block" style="margin-top:-5px"><i class="fas fa-sort-amount-down-alt"></i></button>
                
              <hr class="mt-1">
              <div class="row">
              <?php $__currentLoopData = $produk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-2 col-6">
                    <div class="card text-center" style="height:90%">
                        <div class="card-body">
                            <a href="<?php echo e(route('produk.detail', Crypt::encrypt($pro->id))); ?>" class="d-block mb-2" style="line-height:19px;">
                                <label class="d-block" style="min-height:10px;"><?php echo e(Str::limit($pro->nama_produk, 23)); ?></label>
                                <small><label class="d-block" style="min-height:-5px;"><?php echo e(Str::limit($pro->nama_variant, 23)); ?></label>
                                <label class="d-block" style="min-height:-5px;">warna : <?php echo e(Str::limit($pro->warna, 23)); ?></label>
                                <label class="d-block" style="min-height:-5px;">size : <?php echo e(Str::limit($pro->size, 23)); ?></label></small>
                                <small class="d-block"><?php echo e($pro->stok); ?> tersisa</small>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      <!-- /.col -->
    </div>
    <!-- /.card-body -->
 </div>
  <!-- /.card -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ghighac1/ghigha.my.id/framework/resources/views/admin/produk/index.blade.php ENDPATH**/ ?>